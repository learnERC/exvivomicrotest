#!/bin/bash

function copy_monitor_code() {
	echo "Copying monitor instrumentation code..."

	rsync -raz "monitor/ts-food-map-service/filter" "train-ticket/ts-food-map-service/src/main/java/food/"
	rsync -raz "monitor/ts-food-map-service/config/"*.java "train-ticket/ts-food-map-service/src/main/java/food/config/"
	rsync -raz "monitor/ts-food-map-service/logback-spring.xml" "train-ticket/ts-food-map-service/src/main/resources/logback-spring.xml"
	rsync -raz "monitor/ts-food-map-service/pom.xml" "train-ticket/ts-food-map-service/pom.xml"
	rsync -raz "monitor/ts-food-map-service/Dockerfile" "train-ticket/ts-food-map-service/Dockerfile"

	rsync -raz "monitor/ts-consign-price-service/filter" "train-ticket/ts-consign-price-service/src/main/java/consignprice/"
	rsync -raz "monitor/ts-consign-price-service/config/"*.java "train-ticket/ts-consign-price-service/src/main/java/consignprice/config/"
	rsync -raz "monitor/ts-consign-price-service/logback-spring.xml" "train-ticket/ts-consign-price-service/src/main/resources/logback-spring.xml"
	rsync -raz "monitor/ts-consign-price-service/pom.xml" "train-ticket/ts-consign-price-service/pom.xml"
	rsync -raz "monitor/ts-consign-price-service/Dockerfile" "train-ticket/ts-consign-price-service/Dockerfile"

	rsync -raz "monitor/ts-contacts-service/filter" "train-ticket/ts-contacts-service/src/main/java/contacts/"
	rsync -raz "monitor/ts-contacts-service/config/"*.java "train-ticket/ts-contacts-service/src/main/java/contacts/config/"
	rsync -raz "monitor/ts-contacts-service/logback-spring.xml" "train-ticket/ts-contacts-service/src/main/resources/logback-spring.xml"
	rsync -raz "monitor/ts-contacts-service/pom.xml" "train-ticket/ts-contacts-service/pom.xml"
	rsync -raz "monitor/ts-contacts-service/Dockerfile" "train-ticket/ts-contacts-service/Dockerfile"

	echo "Done!"
}

rm -rf train-ticket
git clone https://github.com/FudanSELab/train-ticket.git
cd train-ticket && git checkout v0.2.0 && cd ..

copy_monitor_code

./prepare_scenario_run.sh

./run_scenario.sh -s 001_scenario -svc ts-food-map-service --monitor
./run_scenario.sh -s 002_scenario -svc ts-food-map-service --monitor
./run_scenario.sh -s 003_scenario -svc ts-food-map-service --monitor
./run_scenario.sh -s 004_scenario -svc ts-food-map-service --monitor

./run_scenario.sh -s 001_scenario -svc ts-consign-price-service --monitor
./run_scenario.sh -s 002_scenario -svc ts-consign-price-service --monitor
./run_scenario.sh -s 003_scenario -svc ts-consign-price-service --monitor
./run_scenario.sh -s 004_scenario -svc ts-consign-price-service --monitor

./run_scenario.sh -s 001_scenario -svc ts-contacts-service --monitor
./run_scenario.sh -s 002_scenario -svc ts-contacts-service --monitor
./run_scenario.sh -s 003_scenario -svc ts-contacts-service --monitor

docker-compose -f train-ticket/docker-compose.custom.yml down

docker rmi $(docker images -f "reference=codewisdom/*" -q)