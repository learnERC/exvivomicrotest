package consignprice.filter.utils;

import consignprice.entity.ConsignPrice;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AbstractionUtils {
  public static final String REQUEST_ABSTRACTION = "0";
  public static final String RESPONSE_ABSTRACTION = "1";

  public static final String GET_PRICE_BY_WEIGHT_AND_REGION_ABSTRACTION = "0";
  public static final String GET_PRICE_INFO_ABSTRACTION = "1";
  public static final String GET_PRICE_CONFIG_ABSTRACTION = "2";
  public static final String MODIFY_PRICE_CONFIG_ABSTRACTION = "3";

  public static final Map<String, String> statusCodeAbstractions = buildStatusCodeAbstractions();
  public static final Map<String, String> numericalAbstractions = buildNumericalAbstractions();

  public static final Map<String, String> stringAbstractions = buildStringAbstractions();
  public static final Map<String, String> mapAbstractions = buildMapAbstractions();
  public static final Map<String, String> listAbstractions = buildListAbstractions();
  public static final Map<String, String> objectRefAbstractions = buildObjectRefAbstractions();

  private static final String NULL = "NULL";
  private static final String EMPTY = "EMPTY";
  private static final String NOT_EMPTY = "NOT_EMPTY";
  private static final String NOT_NULL = "NOT_NULL";

  private static final String LT0 = "LESS_THAN_0";
  private static final String GT0 = "GREATER_THAN_0";
  private static final String EQ0 = "EQUAL_TO_0";

  private static final String INFORMATIONAL_RESPONSE = "1xx";
  private static final String SUCCESSFUL = "2xx";
  private static final String REDIRECTION = "3xx";
  private static final String CLIENT_ERROR = "4xx";
  private static final String SERVER_ERROR = "5xx";

  public static Map<String, String> buildStatusCodeAbstractions() {
    Map<String, String> map = new HashMap<>();

    map.put(INFORMATIONAL_RESPONSE, "0");
    map.put(SUCCESSFUL, "1");
    map.put(REDIRECTION, "2");
    map.put(CLIENT_ERROR, "3");
    map.put(SERVER_ERROR, "4");

    return map;
  }

  private static Map<String, String> buildNumericalAbstractions() {
    Map<String, String> map = new HashMap<>();

    map.put(NULL, "0");
    map.put(LT0, "1");
    map.put(GT0, "2");
    map.put(EQ0, "3");

    return map;
  }

  public static Map<String, String> buildStringAbstractions() {
    Map<String, String> map = new HashMap<>();

    map.put(NULL, "0");
    map.put(EMPTY, "1");
    map.put(NOT_EMPTY, "2");

    return map;
  }

  public static Map<String, String> buildMapAbstractions() {
    Map<String, String> map = new HashMap<>();

    map.put(NULL, "0");
    map.put(EMPTY, "1");
    map.put(NOT_EMPTY, "2");

    return map;
  }

  public static Map<String, String> buildListAbstractions() {
    Map<String, String> map = new HashMap<>();

    map.put(NULL, "0");
    map.put(EMPTY, "1");
    map.put(NOT_EMPTY, "2");

    return map;
  }

  public static Map<String, String> buildObjectRefAbstractions() {
    Map<String, String> map = new HashMap<>();

    map.put(NULL, "0");
    map.put(NOT_NULL, "1");

    return map;
  }

  public static String abstractString(final String string) {
    if (null == string) {
      return stringAbstractions.get(NULL);
    } else if (string.isEmpty()) {
      return stringAbstractions.get(EMPTY);
    } else {
      return stringAbstractions.get(NOT_EMPTY);
    }
  }

  public static String abstractList(final List<?> list) {
    if (null == list) {
      return listAbstractions.get(NULL);
    } else if (list.isEmpty()) {
      return listAbstractions.get(EMPTY);
    } else {
      return listAbstractions.get(NOT_EMPTY);
    }
  }

  public static String abstractStatusCode(final int statusCode) {
    if (statusCode >= 100 && statusCode < 200) {
      return statusCodeAbstractions.get(INFORMATIONAL_RESPONSE);
    } else if (statusCode >= 200 && statusCode < 300) {
      return statusCodeAbstractions.get(SUCCESSFUL);
    } else if (statusCode >= 300 && statusCode < 400) {
      return statusCodeAbstractions.get(REDIRECTION);
    } else if (statusCode >= 400 && statusCode < 500) {
      return statusCodeAbstractions.get(CLIENT_ERROR);
    } else {
      return statusCodeAbstractions.get(SERVER_ERROR);
    }
  }

  public static String abstractConsignPrice(final ConsignPrice consignPrice) {
    String symbol = "";

    if (null == consignPrice.getId()) {
      symbol += objectRefAbstractions.get(NULL);
    } else {
      symbol += objectRefAbstractions.get(NOT_NULL);
    }

    if (consignPrice.getIndex() < 0) {
      symbol += numericalAbstractions.get(LT0);
    } else if (consignPrice.getIndex() > 0) {
      symbol += numericalAbstractions.get(GT0);
    } else {
      symbol += numericalAbstractions.get(EQ0);
    }

    if (consignPrice.getInitialWeight() < 0) {
      symbol += numericalAbstractions.get(LT0);
    } else if (consignPrice.getInitialWeight() > 0) {
      symbol += numericalAbstractions.get(GT0);
    } else {
      symbol += numericalAbstractions.get(EQ0);
    }

    if (consignPrice.getInitialPrice() < 0) {
      symbol += numericalAbstractions.get(LT0);
    } else if (consignPrice.getInitialPrice() > 0) {
      symbol += numericalAbstractions.get(GT0);
    } else {
      symbol += numericalAbstractions.get(EQ0);
    }

    if (consignPrice.getWithinPrice() < 0) {
      symbol += numericalAbstractions.get(LT0);
    } else if (consignPrice.getWithinPrice() > 0) {
      symbol += numericalAbstractions.get(GT0);
    } else {
      symbol += numericalAbstractions.get(EQ0);
    }

    if (consignPrice.getBeyondPrice() < 0) {
      symbol += numericalAbstractions.get(LT0);
    } else if (consignPrice.getBeyondPrice() > 0) {
      symbol += numericalAbstractions.get(GT0);
    } else {
      symbol += numericalAbstractions.get(EQ0);
    }

    return symbol;
  }
}
