package consignprice.filter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import consignprice.entity.ConsignPrice;
import consignprice.filter.utils.AbstractionUtils;
import consignprice.filter.utils.wrapper.SpringRequestWrapper;
import consignprice.filter.utils.wrapper.SpringResponseWrapper;
import edu.fudan.common.util.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

public class MonitorFilter extends OncePerRequestFilter {

  private static final Logger LOGGER = LoggerFactory.getLogger(MonitorFilter.class);
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private static final String API_PREFIX = "/api/v1/consignpriceservice";
  private static final String GET_PRICE_BY_WEIGHT_AND_REGION_PATH = API_PREFIX + "/consignprice/{weight}/{isWithinRegion}";
  private static final String GET_PRICE_INFO_PATH = API_PREFIX + "/consignprice/price";
  private static final String GET_PRICE_CONFIG_PATH = API_PREFIX + "/consignprice/config";
  private static final String MODIFY_PRICE_CONFIG_PATH = API_PREFIX + "/consignprice";

  private final Mode currentMode;
  private final Map<String, BigDecimal> eventSymbolProbabilities;

  public enum Mode {
    DEFAULT,
    LOGGER
  }

  public MonitorFilter(Mode mode) {
    super();
    this.currentMode = mode;
    this.eventSymbolProbabilities = new HashMap<>();
    for(int i = 0; i < 8208; i++) {
      eventSymbolProbabilities.put(Integer.toString(i), BigDecimal.valueOf(0.1));
    }
    OBJECT_MAPPER
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws ServletException, IOException {

    final SpringRequestWrapper wrappedRequest = new SpringRequestWrapper(request);
    final String eventSymbolRequest = buildRequestSymbol(wrappedRequest);

    if (Mode.LOGGER.equals(currentMode)) {
      LOGGER.info("{}", eventSymbolRequest);
    } else {
      shouldActivateTracing(eventSymbolRequest);
    }

    final SpringResponseWrapper wrappedResponse = new SpringResponseWrapper(response);

    try {
      chain.doFilter(wrappedRequest, wrappedResponse);
    } catch (Exception e) {
      wrappedResponse.setStatus(500);
      final String eventSymbolResponse = buildResponseSymbol(wrappedRequest, wrappedResponse);
      if (Mode.LOGGER.equals(currentMode)) {
        LOGGER.info("{}", eventSymbolResponse);
      } else {
        shouldActivateTracing(eventSymbolResponse);
      }
      throw e;
    }

    final String eventSymbolResponse = buildResponseSymbol(wrappedRequest, wrappedResponse);

    if (Mode.LOGGER.equals(currentMode)) {
      LOGGER.info("{}", eventSymbolResponse);
    } else {
      shouldActivateTracing(eventSymbolResponse);
    }

  }

  private boolean shouldActivateTracing(String symbol) {
    final BigDecimal rand = BigDecimal.valueOf(Math.random());
    final BigDecimal eventSymbolProb = eventSymbolProbabilities.get("0");

    if (rand.compareTo(eventSymbolProb) >= 0) {
      return true;
    }
    return false;
  }

  private String buildRequestSymbol(SpringRequestWrapper request) throws IOException {
    final String prefix = AbstractionUtils.REQUEST_ABSTRACTION;

    // Select endpoint
    final String method = request.getMethod();
    final String URI = request.getRequestURI();

    LOGGER.debug("{} {}", method, URI);

    switch (method) {
      case "GET":
        if (GET_PRICE_INFO_PATH.equals(URI)) {
          return prefix + buildGetPriceInfoSymbolRequest(request);
        } else if (GET_PRICE_CONFIG_PATH.equals(URI)) {
          return prefix + buildGetPriceConfigSymbolRequest(request);
        } else if (URI.contains(GET_PRICE_BY_WEIGHT_AND_REGION_PATH.replace("/{weight}", "").replace("/{isWithinRegion}", ""))) {
          return prefix + buildGetPriceByWeightAndRegionSymbolRequest(request);
        }
      case "POST":
        return prefix + buildModifyPriceConfigSymbolRequest(request);
    }

    return prefix;
  }

  private String buildResponseSymbol(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String prefix = AbstractionUtils.RESPONSE_ABSTRACTION;

    // Select endpoint
    final String method = request.getMethod();
    final String URI = request.getRequestURI();

    LOGGER.debug("{} {}", method, URI);

    switch (method) {
      case "GET":
        if (GET_PRICE_INFO_PATH.equals(URI)) {
          return prefix + buildGetPriceInfoSymbolResponse(request, response);
        } else if (GET_PRICE_CONFIG_PATH.equals(URI)) {
          return prefix + buildGetPriceConfigSymbolResponse(request, response);
        } else if (URI.contains(GET_PRICE_BY_WEIGHT_AND_REGION_PATH.replace("/{weight}", "").replace("/{isWithinRegion}", ""))) {
          return prefix + buildGetPriceByWeightAndRegionSymbolResponse(request, response);
        }
      case "POST":
        return prefix + buildModifyPriceConfigSymbolResponse(request, response);
    }

    return prefix;
  }

  private String buildGetPriceByWeightAndRegionSymbolRequest(SpringRequestWrapper request) {
    return AbstractionUtils.GET_PRICE_BY_WEIGHT_AND_REGION_ABSTRACTION;
  }

  private String buildGetPriceByWeightAndRegionSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<ConsignPrice> consignPrice =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<ConsignPrice>>(){});
      if (null != consignPrice.getData()) {
        payload = AbstractionUtils.abstractConsignPrice(consignPrice.getData());
      }
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_PRICE_BY_WEIGHT_AND_REGION_ABSTRACTION + statusCode + payload;
  }

  private String buildGetPriceInfoSymbolRequest(SpringRequestWrapper request) {
    return AbstractionUtils.GET_PRICE_INFO_ABSTRACTION;
  }

  private String buildGetPriceInfoSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<String> priceInfo =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<String>>(){});
      payload = AbstractionUtils.abstractString(priceInfo.getData());
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_PRICE_INFO_ABSTRACTION + statusCode + payload;
  }

  private String buildGetPriceConfigSymbolRequest(SpringRequestWrapper request) {
    return AbstractionUtils.GET_PRICE_CONFIG_ABSTRACTION;
  }

  private String buildGetPriceConfigSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<ConsignPrice> consignPrice =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<ConsignPrice>>(){});
      if (null != consignPrice.getData()) {
        payload = AbstractionUtils.abstractConsignPrice(consignPrice.getData());
      }
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_PRICE_CONFIG_ABSTRACTION + statusCode + payload;
  }

  private String buildModifyPriceConfigSymbolRequest(SpringRequestWrapper request)
      throws IOException {
    String payload = "";
    try {
      final ConsignPrice consignPrice =
          OBJECT_MAPPER.readValue(
              inputStreamToString(
                  request.getInputStream(), request.getCharacterEncoding()), ConsignPrice.class);
      payload = AbstractionUtils.abstractConsignPrice(consignPrice);
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }
    return AbstractionUtils.MODIFY_PRICE_CONFIG_ABSTRACTION + payload;
  }

  private String buildModifyPriceConfigSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<ConsignPrice> consignPrice =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<ConsignPrice>>(){});
      if (null != consignPrice.getData()) {
        payload = AbstractionUtils.abstractConsignPrice(consignPrice.getData());
      }
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.MODIFY_PRICE_CONFIG_ABSTRACTION + statusCode + payload;
  }

  private String inputStreamToString(ServletInputStream inputStream, String characterEncoding)
      throws IOException {
    ByteArrayOutputStream result = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int length;
    while ((length = inputStream.read(buffer)) != -1) {
      result.write(buffer, 0, length);
    }
    if (null == characterEncoding) {
      characterEncoding = "UTF-8";
    }
    return result.toString(characterEncoding);
  }

  private String byteArrayToString(byte[] bytes, String characterEncoding) {
    if (null == characterEncoding) {
      characterEncoding = "UTF-8";
    }
    return new String(bytes, Charset.forName(characterEncoding));
  }

  private void logRequest(SpringRequestWrapper wrappedRequest) throws IOException {
    LOGGER.debug(
        "Request: method={}, uri={}, payload={}",
        wrappedRequest.getMethod(),
        wrappedRequest.getRequestURI(),
        inputStreamToString(
            wrappedRequest.getInputStream(), wrappedRequest.getCharacterEncoding()));
  }

  private void logResponse(SpringRequestWrapper wrappedRequest, SpringResponseWrapper wrappedResponse, int overriddenStatus)
      throws IOException {
    wrappedResponse.setCharacterEncoding("UTF-8");
    LOGGER.debug(
        "Response: method={}, uri={}, status={}, payload={}",
        wrappedRequest.getMethod(),
        wrappedRequest.getRequestURI(),
        overriddenStatus,
        byteArrayToString(
            wrappedResponse.getContentAsByteArray(), wrappedResponse.getCharacterEncoding()));
  }
}
