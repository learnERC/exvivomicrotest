package food.filter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import food.entity.FoodStore;
import food.entity.TrainFood;
import food.filter.utils.AbstractionUtils;
import food.filter.utils.wrapper.SpringRequestWrapper;
import food.filter.utils.wrapper.SpringResponseWrapper;
import edu.fudan.common.util.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

public class MonitorFilter extends OncePerRequestFilter {

  private static final Logger LOGGER = LoggerFactory.getLogger(MonitorFilter.class);
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private static final String API_PREFIX = "/api/v1/foodmapservice";
  private static final String GET_ALL_FOOD_STORES_PATH = API_PREFIX + "/foodstores";
  private static final String GET_FOOD_STORES_OF_STATION_PATH = API_PREFIX + "/foodstores/{stationId}";
  private static final String GET_FOOD_STORES_BY_STATION_IDS_PATH = API_PREFIX + "/foodstores";
  private static final String GET_ALL_TRAIN_FOOD_PATH = API_PREFIX + "/trainfoods";
  private static final String GET_TRAIN_FOOD_OF_TRIP_PATH = API_PREFIX + "/trainfoods/{tripId}";

  private final Mode currentMode;
  private final Map<String, BigDecimal> eventSymbolProbabilities;

  public enum Mode {
    DEFAULT,
    LOGGER
  }

  public MonitorFilter(Mode mode) {
    super();
    this.currentMode = mode;
    this.eventSymbolProbabilities = new HashMap<>();
    for(int i = 0; i < 27; i++) {
      eventSymbolProbabilities.put(Integer.toString(i), BigDecimal.valueOf(0.1));
    }
    OBJECT_MAPPER
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws ServletException, IOException {

    final SpringRequestWrapper wrappedRequest = new SpringRequestWrapper(request);
    final String eventSymbolRequest = buildRequestSymbol(wrappedRequest);

    if (Mode.LOGGER.equals(currentMode)) {
      LOGGER.info("{}", eventSymbolRequest);
    } else {
      shouldActivateTracing(eventSymbolRequest);
    }

    final SpringResponseWrapper wrappedResponse = new SpringResponseWrapper(response);

    try {
      chain.doFilter(wrappedRequest, wrappedResponse);
    } catch (Exception e) {
      wrappedResponse.setStatus(500);
      final String eventSymbolResponse = buildResponseSymbol(wrappedRequest, wrappedResponse);
      if (Mode.LOGGER.equals(currentMode)) {
        LOGGER.info("{}", eventSymbolResponse);
      } else {
        shouldActivateTracing(eventSymbolResponse);
      }
      throw e;
    }

    final String eventSymbolResponse = buildResponseSymbol(wrappedRequest, wrappedResponse);

    if (Mode.LOGGER.equals(currentMode)) {
      LOGGER.info("{}", eventSymbolResponse);
    } else {
      shouldActivateTracing(eventSymbolResponse);
    }

  }

  private boolean shouldActivateTracing(String symbol) {
    final BigDecimal rand = BigDecimal.valueOf(Math.random());
    final BigDecimal eventSymbolProb = eventSymbolProbabilities.get("0");

    if (rand.compareTo(eventSymbolProb) >= 0) {
      return true;
    }
    return false;
  }

  private String buildRequestSymbol(SpringRequestWrapper request) throws IOException {
    final String prefix = AbstractionUtils.REQUEST_ABSTRACTION;

    // Select endpoint
    final String method = request.getMethod();
    final String URI = request.getRequestURI();

    LOGGER.debug("{} {}", method, URI);

    switch (method) {
      case "GET":
        if (GET_ALL_FOOD_STORES_PATH.equals(URI)) {
          return prefix + buildGetAllFoodStoresSymbolRequest(request);
        } else if (URI.contains(GET_FOOD_STORES_OF_STATION_PATH.replace("{stationId}", ""))) {
          return prefix + buildGetFoodStoresOfStationSymbolRequest(request);
        } else if (GET_ALL_TRAIN_FOOD_PATH.equals(URI)) {
          return prefix + buildGetAllTrainFoodSymbolRequest(request);
        } else if (URI.contains(GET_TRAIN_FOOD_OF_TRIP_PATH.replace("{tripId}", ""))) {
          return prefix + buildGetTrainFoodOfTripSymbolRequest(request);
        }
      case "POST":
        if (GET_FOOD_STORES_BY_STATION_IDS_PATH.equals(URI)) {
          return prefix + buildPostGetFoodStoresByStationIdsSymbolRequest(request);
        }
    }

    return prefix;
  }

  private String buildResponseSymbol(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String prefix = AbstractionUtils.RESPONSE_ABSTRACTION;

    // Select endpoint
    final String method = request.getMethod();
    final String URI = request.getRequestURI();

    LOGGER.debug("{} {}", method, URI);

    switch (method) {
      case "GET":
        if (GET_ALL_FOOD_STORES_PATH.equals(URI)) {
          return prefix + buildGetAllFoodStoresSymbolResponse(request, response);
        } else if (URI.contains(GET_FOOD_STORES_OF_STATION_PATH.replace("{stationId}", ""))) {
          return prefix + buildGetFoodStoresOfStationSymbolResponse(request, response);
        } else if (GET_ALL_TRAIN_FOOD_PATH.equals(URI)) {
          return prefix + buildGetAllTrainFoodSymbolResponse(request, response);
        } else if (URI.contains(GET_TRAIN_FOOD_OF_TRIP_PATH.replace("{tripId}", ""))) {
          return prefix + buildGetTrainFoodOfTripSymbolResponse(request, response);
        }
      case "POST":
        if (GET_FOOD_STORES_BY_STATION_IDS_PATH.equals(URI)) {
          return prefix + buildPostGetFoodStoresByStationIdsSymbolResponse(request, response);
        }
    }

    return prefix;
  }

  private String buildGetAllFoodStoresSymbolRequest(SpringRequestWrapper request) {
    return AbstractionUtils.GET_ALL_FOOD_STORES_ABSTRACTION;
  }

  private String buildGetAllFoodStoresSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<List<FoodStore>> foodStores =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<List<FoodStore>>>(){});

      payload = AbstractionUtils.abstractList(foodStores.getData());

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_ALL_FOOD_STORES_ABSTRACTION + statusCode + payload;
  }

  private String buildGetFoodStoresOfStationSymbolRequest(SpringRequestWrapper request) {
    return AbstractionUtils.GET_FOOD_STORES_OF_STATION_ABSTRACTION;
  }

  private String buildGetFoodStoresOfStationSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<List<FoodStore>> foodStores =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<List<FoodStore>>>(){});

      payload = AbstractionUtils.abstractList(foodStores.getData());

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_FOOD_STORES_OF_STATION_ABSTRACTION + statusCode + payload;
  }

  private String buildPostGetFoodStoresByStationIdsSymbolRequest(SpringRequestWrapper request) throws IOException {
    String payload = "";
    try {
      final List<String> ids =
          OBJECT_MAPPER.readValue(
              inputStreamToString(
                  request.getInputStream(), request.getCharacterEncoding()), new TypeReference<List<String>>(){});
      payload = AbstractionUtils.abstractList(ids);
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }
    return AbstractionUtils.GET_FOOD_STORES_BY_STATION_IDS_ABSTRACTION + payload;
  }

  private String buildPostGetFoodStoresByStationIdsSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<List<FoodStore>> foodStores =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<List<FoodStore>>>(){});

      payload = AbstractionUtils.abstractList(foodStores.getData());

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_FOOD_STORES_BY_STATION_IDS_ABSTRACTION + statusCode + payload;
  }

  private String buildGetAllTrainFoodSymbolRequest(SpringRequestWrapper request) {
    return AbstractionUtils.GET_ALL_TRAIN_FOOD_ABSTRACTION;
  }

  private String buildGetAllTrainFoodSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<List<TrainFood>> foodStores =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<List<TrainFood>>>(){});

      payload = AbstractionUtils.abstractList(foodStores.getData());

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_ALL_TRAIN_FOOD_ABSTRACTION + statusCode + payload;
  }

  private String buildGetTrainFoodOfTripSymbolRequest(SpringRequestWrapper request) {
    return AbstractionUtils.GET_TRAIN_FOOD_OF_TRIP_ABSTRACTION;
  }

  private String buildGetTrainFoodOfTripSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<List<TrainFood>> foodStores =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<List<TrainFood>>>(){});

      payload = AbstractionUtils.abstractList(foodStores.getData());

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_TRAIN_FOOD_OF_TRIP_ABSTRACTION + statusCode + payload;
  }

  private String inputStreamToString(ServletInputStream inputStream, String characterEncoding)
      throws IOException {
    ByteArrayOutputStream result = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int length;
    while ((length = inputStream.read(buffer)) != -1) {
      result.write(buffer, 0, length);
    }
    if (null == characterEncoding) {
      characterEncoding = "UTF-8";
    }
    return result.toString(characterEncoding);
  }

  private String byteArrayToString(byte[] bytes, String characterEncoding) {
    if (null == characterEncoding) {
      characterEncoding = "UTF-8";
    }
    return new String(bytes, Charset.forName(characterEncoding));
  }

  private void logRequest(SpringRequestWrapper wrappedRequest) throws IOException {
    LOGGER.debug(
        "Request: method={}, uri={}, payload={}",
        wrappedRequest.getMethod(),
        wrappedRequest.getRequestURI(),
        inputStreamToString(
            wrappedRequest.getInputStream(), wrappedRequest.getCharacterEncoding()));
  }

  private void logResponse(SpringRequestWrapper wrappedRequest, SpringResponseWrapper wrappedResponse, int overriddenStatus)
      throws IOException {
    wrappedResponse.setCharacterEncoding("UTF-8");
    LOGGER.debug(
        "Response: method={}, uri={}, status={}, payload={}",
        wrappedRequest.getMethod(),
        wrappedRequest.getRequestURI(),
        overriddenStatus,
        byteArrayToString(
            wrappedResponse.getContentAsByteArray(), wrappedResponse.getCharacterEncoding()));
  }
}
