package contacts.filter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import contacts.entity.Contacts;
import contacts.filter.utils.AbstractionUtils;
import contacts.filter.utils.wrapper.SpringRequestWrapper;
import contacts.filter.utils.wrapper.SpringResponseWrapper;
import edu.fudan.common.util.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

public class MonitorFilter extends OncePerRequestFilter {

  private static final Logger LOGGER = LoggerFactory.getLogger(MonitorFilter.class);
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private static final String API_PREFIX = "/api/v1/contactservice";
  private static final String GET_ALL_CONTACTS_PATH = API_PREFIX + "/contacts";
  private static final String CREATE_NEW_CONTACTS_PATH = API_PREFIX + "/contacts";
  private static final String CREATE_NEW_CONTACTS_ADMIN_PATH = API_PREFIX + "/contacts/admin";
  private static final String DELETE_CONTACTS_PATH = API_PREFIX + "/contacts/{contactsId}";
  private static final String MODIFY_CONTACTS_PATH = API_PREFIX + "/contacts";
  private static final String FIND_CONTACTS_BY_ACCOUNT_ID_PATH = API_PREFIX + "/contacts/account/{accountId}";
  private static final String GET_CONTACTS_BY_CONTACTS_ID_PATH = API_PREFIX + "/contacts/{id}";

  private final Mode currentMode;
  private final Map<String, BigDecimal> eventSymbolProbabilities;

  public enum Mode {
    DEFAULT,
    LOGGER
  }

  public MonitorFilter(Mode mode) {
    super();
    this.currentMode = mode;
    this.eventSymbolProbabilities = new HashMap<>();
    for(int i = 0; i < 3047; i++) {
      eventSymbolProbabilities.put(Integer.toString(i), BigDecimal.valueOf(0.1));
    }
    OBJECT_MAPPER
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws ServletException, IOException {

    final SpringRequestWrapper wrappedRequest = new SpringRequestWrapper(request);
    final String eventSymbolRequest = buildRequestSymbol(wrappedRequest);

    if (Mode.LOGGER.equals(currentMode)) {
      LOGGER.info("{}", eventSymbolRequest);
    } else {
      shouldActivateTracing(eventSymbolRequest);
    }

    final SpringResponseWrapper wrappedResponse = new SpringResponseWrapper(response);

    try {
      chain.doFilter(wrappedRequest, wrappedResponse);
    } catch (Exception e) {
      wrappedResponse.setStatus(500);
      final String eventSymbolResponse = buildResponseSymbol(wrappedRequest, wrappedResponse);
      if (Mode.LOGGER.equals(currentMode)) {
        LOGGER.info("{}", eventSymbolResponse);
      } else {
        shouldActivateTracing(eventSymbolResponse);
      }
      throw e;
    }

    final String eventSymbolResponse = buildResponseSymbol(wrappedRequest, wrappedResponse);

    if (Mode.LOGGER.equals(currentMode)) {
      LOGGER.info("{}", eventSymbolResponse);
    } else {
      shouldActivateTracing(eventSymbolResponse);
    }

  }

  private boolean shouldActivateTracing(String symbol) {
    final BigDecimal rand = BigDecimal.valueOf(Math.random());
    final BigDecimal eventSymbolProb = eventSymbolProbabilities.get("0");

    if (rand.compareTo(eventSymbolProb) >= 0) {
      return true;
    }
    return false;
  }

  private String buildRequestSymbol(SpringRequestWrapper request) throws IOException {
    final String prefix = AbstractionUtils.REQUEST_ABSTRACTION;

    // Select endpoint
    final String method = request.getMethod();
    final String URI = request.getRequestURI();

    LOGGER.debug("{} {}", method, URI);

    switch (method) {
      case "GET":
        if (GET_ALL_CONTACTS_PATH.equals(URI)) {
          return prefix + buildGetGetAllContactsSymbolRequest(request);
        } else if (URI.contains(FIND_CONTACTS_BY_ACCOUNT_ID_PATH.replace("{accountId}", ""))) {
          return prefix + buildFindContactsByAccountIdSymbolRequest(request);
        } else if (URI.contains(GET_CONTACTS_BY_CONTACTS_ID_PATH.replace("{id}", ""))) {
          return prefix + buildGetContactsByContactsIdSymbolRequest(request);
        }
      case "POST":
        if (CREATE_NEW_CONTACTS_PATH.equals(URI)) {
          return prefix + buildPostCreateNewContactsSymbolRequest(request);
        } else if (CREATE_NEW_CONTACTS_ADMIN_PATH.equals(URI)) {
          return prefix + buildPostCreateNewContactsAdminSymbolRequest(request);
        }
      case "PUT":
        return prefix + buildModifyContactsSymbolRequest(request);
      case "DELETE":
        return prefix + buildDeleteContactsSymbolRequest(request);
    }

    return prefix;
  }

  private String buildResponseSymbol(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String prefix = AbstractionUtils.RESPONSE_ABSTRACTION;

    // Select endpoint
    final String method = request.getMethod();
    final String URI = request.getRequestURI();

    LOGGER.debug("{} {}", method, URI);

    switch (method) {
      case "GET":
        if (GET_ALL_CONTACTS_PATH.equals(URI)) {
          return prefix + buildGetGetAllContactsSymbolResponse(request, response);
        } else if (URI.contains(FIND_CONTACTS_BY_ACCOUNT_ID_PATH.replace("{accountId}", ""))) {
          return prefix + buildFindContactsByAccountIdSymbolResponse(request, response);
        } else if (URI.contains(GET_CONTACTS_BY_CONTACTS_ID_PATH.replace("{id}", ""))) {
          return prefix + buildGetContactsByContactsIdSymbolResponse(request, response);
        }
      case "POST":
        if (CREATE_NEW_CONTACTS_PATH.equals(URI)) {
          return prefix + buildPostCreateNewContactsSymbolResponse(request, response);
        } else if (CREATE_NEW_CONTACTS_ADMIN_PATH.equals(URI)) {
          return prefix + buildPostCreateNewContactsAdminSymbolResponse(request, response);
        }
      case "PUT":
        return prefix + buildModifyContactsSymbolResponse(request, response);
      case "DELETE":
        return prefix + buildDeleteContactsSymbolResponse(request, response);
    }

    return prefix;
  }

  private String buildGetGetAllContactsSymbolRequest(SpringRequestWrapper request) {
    return AbstractionUtils.GET_ALL_CONTACTS_ABSTRACTION;
  }

  private String buildGetGetAllContactsSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<ArrayList<Contacts>> contactsList =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<ArrayList<Contacts>>>(){});
      payload = AbstractionUtils.abstractList(contactsList.getData());
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_ALL_CONTACTS_ABSTRACTION + statusCode + payload;
  }

  private String buildPostCreateNewContactsSymbolRequest(SpringRequestWrapper request) throws IOException {
    String payload = "";
    try {
      final Contacts contacts =
          OBJECT_MAPPER.readValue(
              inputStreamToString(
                  request.getInputStream(), request.getCharacterEncoding()), Contacts.class);
      payload = AbstractionUtils.abstractContacts(contacts);
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }
    return AbstractionUtils.CREATE_NEW_CONTACTS_ABSTRACTION + payload;
  }

  private String buildPostCreateNewContactsSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<Contacts> contacts =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<Contacts>>(){});

      if (null != contacts.getData()) {
        payload = AbstractionUtils.abstractContacts(contacts.getData());
      }

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.CREATE_NEW_CONTACTS_ABSTRACTION + statusCode + payload;
  }

  private String buildPostCreateNewContactsAdminSymbolRequest(SpringRequestWrapper request) throws IOException {
    String payload = "";
    try {
      final Contacts contacts =
          OBJECT_MAPPER.readValue(
              inputStreamToString(
                  request.getInputStream(), request.getCharacterEncoding()), Contacts.class);
      payload = AbstractionUtils.abstractContacts(contacts);
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }
    return AbstractionUtils.CREATE_NEW_CONTACTS_ADMIN_ABSTRACTION + payload;
  }

  private String buildPostCreateNewContactsAdminSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<Contacts> contacts =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<Contacts>>(){});

      if (null != contacts.getData()) {
        payload = AbstractionUtils.abstractContacts(contacts.getData());
      }

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.CREATE_NEW_CONTACTS_ADMIN_ABSTRACTION + statusCode + payload;
  }

  private String buildDeleteContactsSymbolRequest(SpringRequestWrapper request) {
    return AbstractionUtils.DELETE_CONTACTS_ABSTRACTION;
  }

  private String buildDeleteContactsSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<UUID> responsePayload =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<UUID>>(){});

      payload = AbstractionUtils.abstractObjectRef(responsePayload.getData());

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.DELETE_CONTACTS_ABSTRACTION + statusCode + payload;
  }

  private String buildModifyContactsSymbolRequest(SpringRequestWrapper request) throws IOException {
    String payload = "";
    try {
      final Contacts contacts =
          OBJECT_MAPPER.readValue(
              inputStreamToString(
                  request.getInputStream(), request.getCharacterEncoding()), Contacts.class);
      payload = AbstractionUtils.abstractContacts(contacts);
    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }
    return AbstractionUtils.MODIFY_CONTACTS_ABSTRACTION + payload;
  }

  private String buildModifyContactsSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<Contacts> contacts =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<Contacts>>(){});

      if (null != contacts.getData()) {
        payload = AbstractionUtils.abstractContacts(contacts.getData());
      }

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.MODIFY_CONTACTS_ABSTRACTION + statusCode + payload;
  }

  private String buildFindContactsByAccountIdSymbolRequest(SpringRequestWrapper request) throws IOException {
    return AbstractionUtils.FIND_CONTACTS_BY_ACCOUNT_ID_ABSTRACTION;
  }

  private String buildFindContactsByAccountIdSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<List<Contacts>> contacts =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<List<Contacts>>>(){});

      payload = AbstractionUtils.abstractList(contacts.getData());

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.FIND_CONTACTS_BY_ACCOUNT_ID_ABSTRACTION + statusCode + payload;
  }

  private String buildGetContactsByContactsIdSymbolRequest(SpringRequestWrapper request) throws IOException {
    return AbstractionUtils.GET_CONTACTS_BY_CONTACTS_ID_ABSTRACTION;
  }

  private String buildGetContactsByContactsIdSymbolResponse(SpringRequestWrapper request, SpringResponseWrapper response)
      throws IOException {
    final String statusCode = AbstractionUtils.abstractStatusCode(response.getStatus());
    String payload = "";
    try {
      final Response<Contacts> contacts =
          OBJECT_MAPPER.readValue(
              byteArrayToString(
                  response.getContentAsByteArray(), response.getCharacterEncoding()),
              new TypeReference<Response<Contacts>>(){});
              
      if (null != contacts.getData()) {
        payload = AbstractionUtils.abstractContacts(contacts.getData());
      }

    } catch (JsonMappingException | JsonParseException ignored) {
      LOGGER.debug("{}", ignored.getMessage());
    }

    return AbstractionUtils.GET_CONTACTS_BY_CONTACTS_ID_ABSTRACTION + statusCode + payload;
  }

  private String inputStreamToString(ServletInputStream inputStream, String characterEncoding)
      throws IOException {
    ByteArrayOutputStream result = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int length;
    while ((length = inputStream.read(buffer)) != -1) {
      result.write(buffer, 0, length);
    }
    if (null == characterEncoding) {
      characterEncoding = "UTF-8";
    }
    return result.toString(characterEncoding);
  }

  private String byteArrayToString(byte[] bytes, String characterEncoding) {
    if (null == characterEncoding) {
      characterEncoding = "UTF-8";
    }
    return new String(bytes, Charset.forName(characterEncoding));
  }

  private void logRequest(SpringRequestWrapper wrappedRequest) throws IOException {
    LOGGER.debug(
        "Request: method={}, uri={}, payload={}",
        wrappedRequest.getMethod(),
        wrappedRequest.getRequestURI(),
        inputStreamToString(
            wrappedRequest.getInputStream(), wrappedRequest.getCharacterEncoding()));
  }

  private void logResponse(SpringRequestWrapper wrappedRequest, SpringResponseWrapper wrappedResponse, int overriddenStatus)
      throws IOException {
    wrappedResponse.setCharacterEncoding("UTF-8");
    LOGGER.debug(
        "Response: method={}, uri={}, status={}, payload={}",
        wrappedRequest.getMethod(),
        wrappedRequest.getRequestURI(),
        overriddenStatus,
        byteArrayToString(
            wrappedResponse.getContentAsByteArray(), wrappedResponse.getCharacterEncoding()));
  }
}
