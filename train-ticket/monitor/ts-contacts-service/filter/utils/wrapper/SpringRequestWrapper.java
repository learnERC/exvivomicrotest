package contacts.filter.utils.wrapper;

import contacts.filter.MonitorFilter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpringRequestWrapper extends HttpServletRequestWrapper {

  private byte[] body;
  private static final Logger LOGGER = LoggerFactory.getLogger(SpringRequestWrapper.class);

  public SpringRequestWrapper(HttpServletRequest request) {
    super(request);
    try {
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      request.getInputStream().transferTo(buffer);
      buffer.flush();
      body = buffer.toByteArray();
    } catch (IOException ex) {
      body = new byte[0];
    }
  }

  @Override
  public ServletInputStream getInputStream() throws IOException {
    return new ServletInputStream() {
      final ByteArrayInputStream byteArray = new ByteArrayInputStream(body);

      public boolean isFinished() {
        return false;
      }

      public boolean isReady() {
        return true;
      }

      public void setReadListener(ReadListener readListener) {}

      @Override
      public int read() throws IOException {
        return byteArray.read();
      }
    };
  }
}
