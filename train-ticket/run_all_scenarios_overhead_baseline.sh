#!/bin/bash

function run_services() {
	echo "Running services..."

	# Run services
	docker-compose -f train-ticket/docker-compose.custom.yml up -d redis ts-consign-price-mongo ts-food-map-mongo ts-contacts-mongo ts-auth-mongo
	sleep 10
	docker-compose -f train-ticket/docker-compose.custom.yml up -d ts-auth-service
	sleep 10
	docker-compose -f train-ticket/docker-compose.custom.yml up -d ts-consign-price-service ts-food-map-service ts-contacts-service

	echo "Done!"

	echo "Sleeping for 60s to '''ensure''' warm up of run services"
	sleep 60
}

runs=$1;

mkdir -p overhead-experiments/baseline

rm -rf train-ticket
git clone https://github.com/FudanSELab/train-ticket.git
cd train-ticket && git checkout v0.2.0 && cd ..

./prepare_scenario_run.sh

for (( i = 0; i < $runs; i++ )); do
	if [[ $i > 0 ]]; then
		run_services
	fi

	sudo systemctl start logstash.service
	sudo systemctl start metricbeat.service

	./run_scenario.sh -s 001_scenario -svc ts-food-map-service --latency
	./run_scenario.sh -s 002_scenario -svc ts-food-map-service --latency
	./run_scenario.sh -s 003_scenario -svc ts-food-map-service --latency
	./run_scenario.sh -s 004_scenario -svc ts-food-map-service --latency

	mkdir -p overhead-experiments/baseline/ts-food-map-service/run-$i
	mv scenarios/ts-food-map-service/*latency.log overhead-experiments/baseline/ts-food-map-service/run-$i/

	./run_scenario.sh -s 001_scenario -svc ts-consign-price-service --latency
	./run_scenario.sh -s 002_scenario -svc ts-consign-price-service --latency
	./run_scenario.sh -s 003_scenario -svc ts-consign-price-service --latency
	./run_scenario.sh -s 004_scenario -svc ts-consign-price-service --latency

	mkdir -p overhead-experiments/baseline/ts-consign-price-service/run-$i
	mv scenarios/ts-consign-price-service/*latency.log overhead-experiments/baseline/ts-consign-price-service/run-$i/

	./run_scenario.sh -s 001_scenario -svc ts-contacts-service --latency
	./run_scenario.sh -s 002_scenario -svc ts-contacts-service --latency
	./run_scenario.sh -s 003_scenario -svc ts-contacts-service --latency

	mkdir -p overhead-experiments/baseline/ts-contacts-service/run-$i
	mv scenarios/ts-contacts-service/*latency.log overhead-experiments/baseline/ts-contacts-service/run-$i/

	sudo systemctl stop logstash.service
	sudo systemctl stop metricbeat.service

	docker-compose -f train-ticket/docker-compose.custom.yml down
done

docker rmi $(docker images -f "reference=codewisdom/*" -q)
