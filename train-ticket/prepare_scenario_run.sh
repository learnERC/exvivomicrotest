#!/bin/bash

function build_services() {
	echo "Building services..."

	# Package with maven
	cd train-ticket
	mvn -DskipTests package
	cd ..

	# Build services
	docker-compose -f train-ticket/docker-compose.custom.yml build

	echo "Done!"
}

function run_services() {
	echo "Running services..."

	# Run services
	docker-compose -f train-ticket/docker-compose.custom.yml up -d redis ts-consign-price-mongo ts-food-map-mongo ts-contacts-mongo ts-auth-mongo
	sleep 10
	docker-compose -f train-ticket/docker-compose.custom.yml up -d ts-auth-service
	sleep 10
	docker-compose -f train-ticket/docker-compose.custom.yml up -d ts-consign-price-service ts-food-map-service ts-contacts-service

	echo "Done!"

	echo "Sleeping for 60s to '''ensure''' warm up of run services"
	sleep 60
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
		-nb|--no-build) no_build=1 ;;
        -nr|--no-run) no_run=1;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

rsync --update -raz docker-compose.custom.yml train-ticket/

if [[ -z $no_build ]]; then
	build_services
fi

if [[ -z $no_run ]]; then
	run_services
fi