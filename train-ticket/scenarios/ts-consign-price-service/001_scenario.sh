#!/bin/bash

dirname=`dirname "$0"`
ts_consign_price_service_host=$1
ts_auth_service_host=$2

sleep 2

token=$(curl --silent -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d @$dirname/credentials.json http://$ts_auth_service_host:12340/api/v1/users/login | jq -j .data.token)

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X GET -H "Authorization: Bearer $token" -H "Accept: application/json" http://$ts_consign_price_service_host:16110/api/v1/consignpriceservice/consignprice/10.0/true

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X GET -H "Authorization: Bearer $token" -H "Accept: application/json" http://$ts_consign_price_service_host:16110/api/v1/consignpriceservice/consignprice/10.0/false

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X GET -H "Authorization: Bearer $token" -H "Accept: application/json" http://$ts_consign_price_service_host:16110/api/v1/consignpriceservice/consignprice/price