#!/bin/bash

dirname=`dirname "$0"`
ts_contacts_service_host=$1
ts_auth_service_host=$2

sleep 2

token=$(curl --silent -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d @$dirname/credentials.json http://$ts_auth_service_host:12340/api/v1/users/login | jq -j .data.token)

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X POST -H "Authorization: Bearer $token" -H "Content-Type: application/json" -H "Accept: application/json" -d @$dirname/002_scenario_data.json http://$ts_contacts_service_host:12347/api/v1/contactservice/contacts

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X GET -H "Authorization: Bearer $token" -H "Accept: application/json" http://$ts_contacts_service_host:12347/api/v1/contactservice/contacts

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X PUT -H "Authorization: Bearer $token" -H "Content-Type: application/json" -H "Accept: application/json" -d @$dirname/002_scenario_data_modify.json http://$ts_contacts_service_host:12347/api/v1/contactservice/contacts

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X GET -H "Authorization: Bearer $token" -H "Accept: application/json" http://$ts_contacts_service_host:12347/api/v1/contactservice/contacts/c37b703d-4732-4fb3-b0c3-9eb368959d9b

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X GET -H "Authorization: Bearer $token" -H "Accept: application/json" http://$ts_contacts_service_host:12347/api/v1/contactservice/contacts/5f7939e6-0bea-495d-a892-1bc1717f9854

