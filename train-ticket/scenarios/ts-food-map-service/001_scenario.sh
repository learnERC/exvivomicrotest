#!/bin/bash

dirname=`dirname "$0"`
ts_food_map_service_host=$1

sleep 2

curl -w "%{time_total}\n" --silent -o /dev/null -X GET -H "Accept: application/json" http://$ts_food_map_service_host:18855/api/v1/foodmapservice/foodstores

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X GET -H "Accept: application/json" http://$ts_food_map_service_host:18855/api/v1/foodmapservice/foodstores/nanjing

sleep 5

curl -w "%{time_total}\n" --silent -o /dev/null -X GET -H "Accept: application/json" http://$ts_food_map_service_host:18855/api/v1/foodmapservice/foodstores/shanghai