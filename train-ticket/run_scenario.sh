#!/bin/bash

# Usage: ./run_scenario.sh -s scenario_name -svc service_name [--monitor] [--trace] [--latency]

function main() {

	while [[ "$#" -gt 0 ]]; do
	    case $1 in
	        -s|--scenario) scenario_name="$2"; shift ;;
			-svc|--service) service_name="$2"; shift ;;
			-m|--monitor) monitor=1;;
			-t|--trace) trace=1 ;;
			-l|--latency) latency=1 ;;
	        *) echo "Unknown parameter passed: $1"; exit 1 ;;
	    esac
	    shift
	done

	# Retrieve ts-food-map-service IP
	ts_food_map_service_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps | grep train-ticket_ts-food-map-service | awk '{print $1}'))
	count=1
	while [ -z "$ts_food_map_service_ip" ]
	do
		if (( $count > 50 )); then
			echo "No IP address found for ts-food-map-service after 50 retries"
			exit 1
		else
		    sleep 10
			ts_food_map_service_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps | grep train-ticket_ts-food-map-service | awk '{print $1}'))
			count=$((count+1))
		fi
	done

	# Retrieve ts-consign-price-service IP
	ts_consign_price_service_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps | grep train-ticket_ts-consign-price-service | awk '{print $1}'))
	count=1
	while [ -z "$ts_consign_price_service_ip" ]
	do
		if (( $count > 50 )); then
			echo "No IP address found for ts-consign-price-service after 50 retries"
			exit 1
		else
		    sleep 10
			ts_consign_price_service_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps | grep train-ticket_ts-consign-price-service | awk '{print $1}'))
			count=$((count+1))
		fi
	done

	# Retrieve ts-contacts-service IP
	ts_contacts_service_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps | grep train-ticket_ts-contacts-service | awk '{print $1}'))
	count=1
	while [ -z "$ts_contacts_service_ip" ]
	do
		if (( $count > 50 )); then
			echo "No IP address found for ts-contacts-service after 50 retries"
			exit 1
		else
		    sleep 10
			ts_contacts_service_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps | grep train-ticket_ts-contacts-service | awk '{print $1}'))
			count=$((count+1))
		fi
	done

	# Retrieve ts-auth-service IP
	ts_auth_service_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps | grep train-ticket_ts-auth-service | awk '{print $1}'))
	count=1
	while [ -z "$ts_auth_service_ip" ]
	do
		if (( $count > 50 )); then
			echo "No IP address found for ts-auth-service after 50 retries"
			exit 1
		else
		    sleep 10
			ts_auth_service_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps | grep train-ticket_ts-auth-service | awk '{print $1}'))
			count=$((count+1))
		fi
	done

	echo "ts-food-map-service IP: $ts_food_map_service_ip"
	echo "ts-consign-price-service IP: $ts_consign_price_service_ip"
	echo "ts-contacts-service IP: $ts_contacts_service_ip"
	echo "ts-auth-service IP: $ts_auth_service_ip"

	if [ -n "$ts_food_map_service_ip" ]; then
		printf "Trying to connect to ts-food-map-service ($ts_food_map_service_ip) "
		until [ $(curl -s -o /dev/null -w "%{http_code}" $ts_food_map_service_ip:18855/api/v1/foodmapservice/foodstores/welcome) != "000" ]; do
		    printf '.'
		    sleep 10
		done
		echo "Connected to ts-food-map-service"
	fi

	if [ -n "$ts_consign_price_service_ip" ]; then
		printf "Trying to connect to ts-consign-price-service ($ts_consign_price_service_ip) "
		until [ $(curl -s -o /dev/null -w "%{http_code}" $ts_consign_price_service_ip:16110/api/v1/consignpriceservice/consignprice/welcome) != "000" ]; do
		    printf '.'
		    sleep 10
		done
		echo "Connected to ts-consign-price-service"
	fi

	if [ -n "$ts_contacts_service_ip" ]; then
		printf "Trying to connect to ts-contacts-service ($ts_contacts_service_ip) "
		until [ $(curl -s -o /dev/null -w "%{http_code}" $ts_contacts_service_ip:12347/api/v1/contactservice/contacts/welcome) != "000" ]; do
		    printf '.'
		    sleep 10
		done
		echo "Connected to ts-contacts-service"
	fi

		if [ -n "$ts_auth_service_ip" ]; then
		printf "Trying to connect to ts-auth-service ($ts_auth_service_ip) "
		until [ $(curl -s -o /dev/null -w "%{http_code}" $ts_auth_service_ip:12340/api/v1/auth/hello) != "000" ]; do
		    printf '.'
		    sleep 10
		done
		echo "Connected to ts-auth-service"
	fi

	if [[ $service_name == "ts-food-map-service" ]]; then
		service_ip=$ts_food_map_service_ip
	elif [[ $service_name == "ts-consign-price-service" ]]; then
		service_ip=$ts_consign_price_service_ip
	elif [[ $service_name == "ts-contacts-service" ]]; then
		service_ip=$ts_contacts_service_ip
	fi

	if [[ -n $monitor ]]; then
		clean_monitor_logs
	fi


	if [[ -n $trace ]]; then
		start_tracing
	fi

	echo "Running scenario $scenario_name..."
	if [[ -n $latency ]]; then
		echo "Enabled latency log"
		./scenarios/$service_name/$scenario_name.sh $service_ip $ts_auth_service_ip > ./scenarios/$service_name/$scenario_name-latency.log
	else
		./scenarios/$service_name/$scenario_name.sh $service_ip $ts_auth_service_ip
	fi
	echo "Done!"

	if [[ -n $trace ]]; then
		stop_tracing
	fi

	if [[ -n $monitor ]]; then
		get_monitor_logs
	fi
}

function start_tracing() {
	# Get Docker bridge network interface
	interface=$(sudo brctl show | awk 'NF>1 && NR>1 {print $1}' | grep br-)
	echo "Interface $interface selected for tracing"

	# Run tracing in background for the selected service
	sudo tcpdump -U -i $interface -n "dst host $service_ip or src host $service_ip" -w "./scenarios/$service_name/$scenario_name.pcap" &
}

function stop_tracing() {
	# Kill background tracing process
	pid=$(ps -e | pgrep tcpdump)
	sudo kill -2 $pid
	echo "Tracing stopped"
}

function get_monitor_logs() {
	echo "Copying monitor logs..."
	docker cp "$(docker-compose -f train-ticket/docker-compose.custom.yml ps -q $service_name)":/logs/monitor.log "./scenarios/$service_name/$scenario_name.log"
	docker cp "$(docker-compose -f train-ticket/docker-compose.custom.yml ps -q $service_name)":/logs/monitor-debug.log "./scenarios/$service_name/$scenario_name-debug.log"
	echo "Done!"
}

function clean_monitor_logs() {
	echo "Cleaning monitor logs..."
	docker-compose -f train-ticket/docker-compose.custom.yml exec $service_name sh -c "echo > /logs/monitor.log"
	docker-compose -f train-ticket/docker-compose.custom.yml exec $service_name sh -c "echo > /logs/monitor-debug.log"
	echo "Done!"
}

main "$@"; exit
