#!/bin/bash

function copy_monitor_code() {
	echo "Copying monitor instrumentation code..."

	rsync -raz "monitor/ts-food-map-service/filter" "train-ticket/ts-food-map-service/src/main/java/food/"
	rsync -raz "monitor/ts-food-map-service/config/"*.java "train-ticket/ts-food-map-service/src/main/java/food/config/"
	rsync -raz "monitor/ts-food-map-service/logback-spring.xml" "train-ticket/ts-food-map-service/src/main/resources/logback-spring.xml"
	rsync -raz "monitor/ts-food-map-service/pom.xml" "train-ticket/ts-food-map-service/pom.xml"
	rsync -raz "monitor/ts-food-map-service/Dockerfile" "train-ticket/ts-food-map-service/Dockerfile"
	sed -i 's/LOGGER/DEFAULT/' "train-ticket/ts-food-map-service/src/main/java/food/config/MonitorFilterConfig.java"

	rsync -raz "monitor/ts-consign-price-service/filter" "train-ticket/ts-consign-price-service/src/main/java/consignprice/"
	rsync -raz "monitor/ts-consign-price-service/config/"*.java "train-ticket/ts-consign-price-service/src/main/java/consignprice/config/"
	rsync -raz "monitor/ts-consign-price-service/logback-spring.xml" "train-ticket/ts-consign-price-service/src/main/resources/logback-spring.xml"
	rsync -raz "monitor/ts-consign-price-service/pom.xml" "train-ticket/ts-consign-price-service/pom.xml"
	rsync -raz "monitor/ts-consign-price-service/Dockerfile" "train-ticket/ts-consign-price-service/Dockerfile"
	sed -i 's/LOGGER/DEFAULT/' "train-ticket/ts-consign-price-service/src/main/java/consignprice/config/MonitorFilterConfig.java"

	rsync -raz "monitor/ts-contacts-service/filter" "train-ticket/ts-contacts-service/src/main/java/contacts/"
	rsync -raz "monitor/ts-contacts-service/config/"*.java "train-ticket/ts-contacts-service/src/main/java/contacts/config/"
	rsync -raz "monitor/ts-contacts-service/logback-spring.xml" "train-ticket/ts-contacts-service/src/main/resources/logback-spring.xml"
	rsync -raz "monitor/ts-contacts-service/pom.xml" "train-ticket/ts-contacts-service/pom.xml"
	rsync -raz "monitor/ts-contacts-service/Dockerfile" "train-ticket/ts-contacts-service/Dockerfile"
	sed -i 's/LOGGER/DEFAULT/' "train-ticket/ts-contacts-service/src/main/java/contacts/config/MonitorFilterConfig.java"

	echo "Done!"
}

function run_services() {
	echo "Running services..."

	# Run services
	docker-compose -f train-ticket/docker-compose.custom.yml up -d redis ts-consign-price-mongo ts-food-map-mongo ts-contacts-mongo ts-auth-mongo
	sleep 10
	docker-compose -f train-ticket/docker-compose.custom.yml up -d ts-auth-service
	sleep 10
	docker-compose -f train-ticket/docker-compose.custom.yml up -d ts-consign-price-service ts-food-map-service ts-contacts-service

	echo "Done!"

	echo "Sleeping for 60s to '''ensure''' warm up of run services"
	sleep 60
}

runs=$1;

mkdir -p overhead-experiments/monitor

rm -rf train-ticket
git clone https://github.com/FudanSELab/train-ticket.git
cd train-ticket && git checkout v0.2.0 && cd ..

copy_monitor_code

./prepare_scenario_run.sh

for (( i = 0; i < $runs; i++ )); do
	if [[ $i > 0 ]]; then
		run_services
	fi

	sudo systemctl start logstash.service
	sudo systemctl start metricbeat.service

	./run_scenario.sh -s 001_scenario -svc ts-food-map-service --monitor --latency
	./run_scenario.sh -s 002_scenario -svc ts-food-map-service --monitor --latency
	./run_scenario.sh -s 003_scenario -svc ts-food-map-service --monitor --latency
	./run_scenario.sh -s 004_scenario -svc ts-food-map-service --monitor --latency

	mkdir -p overhead-experiments/monitor/ts-food-map-service/run-$i
	mv scenarios/ts-food-map-service/*latency.log overhead-experiments/monitor/ts-food-map-service/run-$i/

	./run_scenario.sh -s 001_scenario -svc ts-consign-price-service --monitor --latency
	./run_scenario.sh -s 002_scenario -svc ts-consign-price-service --monitor --latency
	./run_scenario.sh -s 003_scenario -svc ts-consign-price-service --monitor --latency
	./run_scenario.sh -s 004_scenario -svc ts-consign-price-service --monitor --latency

	mkdir -p overhead-experiments/monitor/ts-consign-price-service/run-$i
	mv scenarios/ts-consign-price-service/*latency.log overhead-experiments/monitor/ts-consign-price-service/run-$i/

	./run_scenario.sh -s 001_scenario -svc ts-contacts-service --monitor --latency
	./run_scenario.sh -s 002_scenario -svc ts-contacts-service --monitor --latency
	./run_scenario.sh -s 003_scenario -svc ts-contacts-service --monitor --latency


	mkdir -p overhead-experiments/monitor/ts-contacts-service/run-$i
	mv scenarios/ts-contacts-service/*latency.log overhead-experiments/monitor/ts-contacts-service/run-$i/

	sudo systemctl stop logstash.service
	sudo systemctl stop metricbeat.service

	docker-compose -f train-ticket/docker-compose.custom.yml down
done

docker rmi $(docker images -f "reference=codewisdom/*" -q)
