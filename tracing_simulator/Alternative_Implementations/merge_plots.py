import os, sys, pickle
import matplotlib.pyplot as plt
#import matplotlib

def main(argv):
    log_files = list()
    output_name = argv[1]
    y_label = argv[2]
    for i in range(3, len(argv)):
        log_files.append(argv[i])

    #i = 2
    #i = 100
    i = 0
    labels = ["Lazy", "Medium", "Eager"]
    markers = ["o", "^", "s", "D"]
    j = 0
    for log_file in log_files:
        with open(log_file, "rb") as f:
            log_list = pickle.load(f)
        #plt.plot(log_list, label="update rate = {}".format(i), marker=markers[j], markevery=1000000)
        plt.plot(log_list, label=labels[i], marker=markers[i], markevery=1000000)
        i += 1
        #i *= 10
        #j += 1
    plt.legend()
    plt.ticklabel_format(style="plain")
    plt.xlabel("Total Events")
    plt.ylabel(y_label)
    # plt.ylim([0, 110]) # Comment for Traced Events / Uncomment for Covered Sequences as in the paper

    plt.savefig(output_name, bbox_inches='tight', transparent="True", pad_inches=0.1)
    plt.close()
    # with open("Merged_plot.log", "wb") as outfile:
    #     pickle.dump(average.tolist(), outfile)

if __name__ == "__main__":
    main(sys.argv)
