import sys, time, random, pickle, bz2, collections, os, json

from numpy.lib.function_base import cov
from Tracing_Profile import Tracing_Profile
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import numpy as np
import numpy.ma as ma
from itertools import zip_longest
from tables import *


class Tracer_h5:

    def load_sequences(sequences_pickle_folder_path):
        pickle_files_list = [os.path.join(sequences_pickle_folder_path, f) for f in listdir(sequences_pickle_folder_path) if isfile(join(sequences_pickle_folder_path, f)) and (f.endswith(".p") or f.endswith(".h5"))]
        #all_sequences = {}
        all_sequences_numbers = {}
        possible_requests = list()
        possible_responses = list()
        for pickle_file_path in pickle_files_list:
            if "support" in pickle_file_path or "req_res" in pickle_file_path:
                with open(pickle_file_path, "rb") as f:
                    pickle_obj = pickle.load(f)
                    if "support" in pickle_file_path:
                        all_sequences_numbers = pickle_obj
                    else:
                        tuple = pickle_obj
                        possible_requests = tuple[0]
                        possible_responses = tuple[1]
            else:
                    all_sequences_h5 = open_file(pickle_file_path, mode="r")
                    all_sequences = all_sequences_h5.root.Sequences
                    seq_length = all_sequences_h5.root.extras[0]["length"]
        return all_sequences, all_sequences_numbers, possible_requests, possible_responses, seq_length
 
    def _init_ptracing_map(self, p):
        p_tracing_map = {}
        for request in self.possible_requests:
            p_tracing_map[request] = p
        for response in self.possible_responses:
            p_tracing_map[response] = p
        return p_tracing_map

    def _get_initial_penalty_map(self):
            penalty_map = {}
            for request in self.possible_requests:
                penalty_map[request] = 1
            for response in self.possible_responses:
                penalty_map[response] = 1
            return penalty_map

    def __init_everything(self):
        p_tracing_map = self._init_ptracing_map(self.p_max)
        self.penalty_map = self._get_initial_penalty_map()
        self.tracing_profile = Tracing_Profile(p_tracing_map)
        # tmp_map = self.all_sequences
        # _L = 0
        # while True:
        #     if len(tmp_map.values()) > 0:
        #         _L += 1
        #         tmp_map = list(tmp_map.values())[0]
        #     else:
        #         break        
        # self.L = _L
        # self.M = self.L*2 ## DA VERIFICARE

        self.tracing = False
        self.traced_events_number_log = list()
        self.total_traced_events = 0
        self.covered_sequences_number_log = list()
        self.total_covered_sequences_numbers = {}
        self.traced_events_buffer = None
        self.next_e_map = collections.defaultdict(set)
        self.covered_sequences = {}
        self.current_update_rate = 0
        self.p_e_probabilities = {}
        self.activation_event = None

        #self._init_p_e_probabilities()

    def __init__(self, all_sequences, all_sequences_number, p_max, p_min, update_rate, possible_requests, possible_responses, seq_length):
        self.possible_requests = possible_requests
        self.possible_responses = possible_responses
        self.all_sequences = all_sequences
        self.all_sequences_number = all_sequences_number
        self.p_max = p_max
        self.p_min = p_min
        self.update_rate = update_rate
        self.p_tracing_log = list()
        self.L = seq_length
        self.M = self.L*2 ## DA VERIFICARE
        self.__init_everything()


    def _is_request(event):
        return event[0] == "0"

    def start_tracing(self):
        self.events_buffer = tuple()
        self.tracing = True
    
    def _find_sequence(self, sequence):
        new_sequence_str = "".join(sequence)
        condition = "sequence_str == new_sequence_str"
        found = [ x for x in self.all_sequences.where(condition, condvars={"new_sequence_str": new_sequence_str})]
        if len(found) > 0:
            return True
        return False
        
    def _get_possible_sequences(self):
        possible_sequences = set()
        for i in range(self.M-self.L+1):
            possible_sequence = tuple()
            possible_sequence += self.events_buffer[i:self.L+i]
            if Tracer_h5._is_request(possible_sequence[0]):
                if self._find_sequence(possible_sequence):
                    possible_sequences.add(possible_sequence)
        return possible_sequences

    def add_to_covered_sequences(self, new_sequences):
        new_sequences_number = 0
        for new_sequence in new_sequences:
            temp_map = self.covered_sequences
            is_new_sequence = False
            for tuple in new_sequence:
                if tuple in temp_map.keys():
                    temp_map = temp_map[tuple]
                else:
                    temp_map[tuple] = {}
                    temp_map = temp_map[tuple]
                    is_new_sequence = True

            if is_new_sequence:
                new_sequences_number += 1
                if new_sequence[0] in self.total_covered_sequences_numbers.keys():
                    self.total_covered_sequences_numbers[new_sequence[0]] += 1
                else:
                    self.total_covered_sequences_numbers[new_sequence[0]] = 1
            for i in range(len(new_sequence)-1):
                self.next_e_map[new_sequence[i]].add(new_sequence[i+1])
            return new_sequences_number

    def stop_tracing(self):
        self.tracing = False
        possible_sequences = self._get_possible_sequences()
        
        new_sequences_number = self.add_to_covered_sequences(possible_sequences)
        self.update_penalty_rate(new_sequences_number)
        self.events_buffer = None

    def update_penalty_rate(self, new_sequences_number):
        new_penalty_value = 1
        penalty_rate = 0.9
        if new_sequences_number == 0:
            new_penalty_value = self.penalty_map[self.activation_event] * penalty_rate
        self.penalty_map[self.activation_event] = new_penalty_value

    def update_p_e_probabilities(self, e):
        non_covered_sequences_e =  self.all_sequences_number[e] - self.total_covered_sequences_numbers[e]
        self.p_e_probabilities[e] = non_covered_sequences_e / self.all_sequences_number[e]

    def update_tracing_profile(self):
        for e in self.covered_sequences.keys():
            self.update_p_e_probabilities(e)
        
        new_p_tracing_map = self._init_ptracing_map(self.p_min)
        for next_key in self.next_e_map:
            max_p_e = 0
            next_e_list = self.next_e_map[next_key]
            for e in next_e_list:
                if Tracer_h5._is_request(e) and e in self.p_e_probabilities.keys() and max_p_e < self.p_e_probabilities[e]:
                    max_p_e = self.p_e_probabilities[e]
            p_tracing = self.p_min + max_p_e * (self.p_max - self.p_min)
            new_p_tracing_map[next_key] = p_tracing
        new_p_tracing_map = dict((key, value * self.penalty_map[key]) for key, value in new_p_tracing_map.items())
        self.tracing_profile.p_tracing_map = new_p_tracing_map

    def check_tracing_profile_update(self):
        #if len(self.covered_sequences_number_log) > 1:
            #self.current_update_rate += self.covered_sequences_number_log[-1] - self.covered_sequences_number_log[-2]
        self.current_update_rate += 1
        if self.current_update_rate >= self.update_rate:
             self.update_tracing_profile()
             self.current_update_rate = 0

    def read_event(self, event):
        if self.tracing:
            self.p_tracing_log.append(self.p_tracing_log[-1])
            self.total_traced_events +=1
            self.events_buffer += (event,)
            if len(self.events_buffer) == self.M:
                self.stop_tracing()
        else:
            random_prob = random.random()
            #if event in self.tracing_profile.p_tracing_map.keys():
            p_tracing = self.tracing_profile.p_tracing_map[event]# * self.penalty_map[event]
            self.p_tracing_log.append(p_tracing)
            if p_tracing > random_prob:
                self.start_tracing()
                self.activation_event = event
        
        self.traced_events_number_log.append(self.total_traced_events)
        self.covered_sequences_number_log.append(sum(self.total_covered_sequences_numbers.values()))
        self.check_tracing_profile_update()


def compute_metrics_and_save(covered_sequences_number_log_dict, traced_events_number_log_dict, p_tracing_map_log_lists, p_tracing_running_mean_list, file_prefix, perfect_run_path, run_for):
    data = np.array(list(covered_sequences_number_log_dict.values()))
    average = np.average(data, axis=0)
    max_covered_sequences_number = np.max(data)
    #plt.plot(average.tolist(), label = "Avg Covered")
    perfect_run_h5 = open_file(perfect_run_path, mode="r", title="Test file")
    perfect_run_table = perfect_run_h5.root.count
    perfect_run_set = [x['count'] for x in perfect_run_table.iterrows()]
    perfect_run = np.array(perfect_run_set)
    
    percentage = (average/perfect_run)*100
    plt.plot(percentage.tolist(), label = "Avg Covered")
    print("{} - Max Number of Covered Sequences found per iteration: {} ({} in trace)".format(file_prefix, max_covered_sequences_number, perfect_run[-1]))
    plt.legend()
    plt.xlabel("Total Events")
    plt.ylabel("Covered Sequences (%)")
    plt.savefig(file_prefix + "_Covered_Sequences.pdf", bbox_inches='tight', transparent="True", pad_inches=0)
    plt.close()    
    with open(file_prefix + "_Covered_Sequences.log", "wb") as outfile:
        pickle.dump(percentage.tolist(), outfile)
    #     json.dump(covered_sequences_number_log_dict, outfile)
    del covered_sequences_number_log_dict
    del data
    del average
    del percentage


    

    data = np.array(list(traced_events_number_log_dict.values()))
    average = np.average(data, axis=0)
    plt.plot(average.tolist(), label="Avg Traced Events")
    #traced_events_number_log_dict["avg"] = average.tolist()
    #for key, values in traced_events_number_log_dict.items():
        #plt.plot(values, label=key)
    plt.legend()
    plt.xlabel("Total Events")
    plt.ylabel("Number of Traced Events")
    plt.savefig(file_prefix + "_Traced_events.pdf", bbox_inches='tight', transparent="True", pad_inches=0)
    plt.close()
    with open(file_prefix + "_Traced_events.log", "wb") as outfile:
        pickle.dump(average.tolist(), outfile)
    #     json.dump(traced_events_number_log_dict, outfile)
    del traced_events_number_log_dict
    del data
    del average

    
    data = np.array(list(p_tracing_map_log_lists))
    average = np.average(data, axis=0)
    plt.plot(average.tolist(), label="Avg P_Tracing Mean")
    plt.legend()
    plt.xlabel("Total Events")
    plt.ylabel("p_tracing")
    plt.savefig(file_prefix + "_P_Tracing_Overall_Mean.pdf", bbox_inches='tight', transparent="True", pad_inches=0)
    plt.close()
    with open(file_prefix + "_P_Tracing_Overall_Mean.log", "wb") as outfile:
        pickle.dump(average.tolist(), outfile)
    #     json.dump(p_tracing_map_log_dict, outfile)
    del p_tracing_map_log_lists

    data = np.array(p_tracing_running_mean_list)
    average = np.average(data, axis=0)
    plt.plot(average.tolist(), label="Avg P_Tracing Running Mean")
    #for key, values in p_tracing_running_mean_dict.items():
     #    plt.plot(values, label=key)
    plt.legend()
    plt.xlabel("Total Events")
    plt.ylabel("p_tracing")
    plt.savefig(file_prefix + "_P_Tracing_running_mean.pdf", bbox_inches='tight', transparent="True", pad_inches=0)
    plt.close()
    with open(file_prefix + "_P_Tracing_running_mean.log", "wb") as outfile:
        pickle.dump(average.tolist(), outfile)
    #     json.dump(p_tracing_running_mean_dict, outfile)
    del p_tracing_running_mean_list



##############



def main(argv):

    arg_names = ["command", "trace_to_execute_path", "perfect_run_path", "number_of_runs", "possible_sequences_folder_path", "p_min", "p_max", "update_rate", "run_for"]
    args = dict(zip(arg_names, argv))
    Arg_list = collections.namedtuple('Arg_list', arg_names)
    args = Arg_list(*(args.get(arg, None) for arg in arg_names))
   
    with open(args.trace_to_execute_path, "r") as infile:
        trace_to_execute = json.load(infile)
    
    if args.run_for and int(args.run_for) < len(trace_to_execute):
        trace_to_execute = trace_to_execute[:int(args.run_for)]

    all_sequences, all_sequences_numbers, possible_requests, possible_responses, seq_length = Tracer_h5.load_sequences(args.possible_sequences_folder_path)

    covered_sequences_number_log_dict = {}
    traced_events_number_log_dict = {}
    p_tracing_map_log_lists = list()
    p_tracing_running_mean_list = list()
    for i in range(int(args.number_of_runs)):
        tracer = Tracer_h5(all_sequences, all_sequences_numbers, float(args.p_max), float(args.p_min), int(args.update_rate), possible_requests, possible_responses, seq_length)
        start_time = time.time()
        p_tracing_map_log_list = list()
        for event in trace_to_execute:
            tracer.read_event(event)
            #p_tracing_map_mean_value = np.mean(list(tracer.tracing_profile.p_tracing_map.values()))
            p_tracing_map_log_list.append(tracer.tracing_profile.p_tracing_map_mean_value)
        covered_sequences_number_log_dict[str(i)] = tracer.covered_sequences_number_log
        traced_events_number_log_dict[str(i)] = tracer.traced_events_number_log

        N = tracer.update_rate
        p_tracing_running_mean_list.append(np.convolve(tracer.p_tracing_log, np.ones(N)/N, mode='valid').tolist())
        p_tracing_map_log_lists.append(p_tracing_map_log_list)
        print("Iteration {} finished --- {} seconds ---".format(i, time.time() - start_time))
    del tracer
    del all_sequences
    
    compute_metrics_and_save(covered_sequences_number_log_dict, traced_events_number_log_dict, p_tracing_map_log_lists, p_tracing_running_mean_list, args.trace_to_execute_path, args.perfect_run_path, int(args.run_for))



if __name__ == "__main__":
    main(sys.argv)