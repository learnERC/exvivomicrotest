import os, sys, pickle
import matplotlib.pyplot as plt
import numpy as np
#import matplotlib

services4 = ["notification_service", "statistics_service", "ts_food_map_service", "account_service"]
services3 = ["ts_contacts_service", "ts_consign_price_service"]

lazy_path = "/media/kralizec/Data2/JSEP_Simulator/experiment_run_0.01_0.05_1000"
medium_path = "/media/kralizec/Data2/JSEP_Simulator/experiment_run_0.03_0.1_1000"
eager_path = "/media/kralizec/Data2/JSEP_Simulator/experiment_run_0.05_0.2_1000"

experiments = [lazy_path, medium_path, eager_path]


# def _get_specific_trace(folder_path, look_for):
#     to_ret = {}
#     services4_paths = [os.path.join(folder_path, f)for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f)) and ".log" in f and look_for in f]
#     to_ret["S1"] = [trace_file for trace_file in services4_paths if "S1" in trace_file][0]
#     to_ret["S2"] = [trace_file for trace_file in services4_paths if "S2" in trace_file][0]
#     to_ret["S3"] = [trace_file for trace_file in services4_paths if "S3" in trace_file][0]
#     return to_ret

# def get_covered_sequences(folder_path):
#     return _get_specific_trace(folder_path, "Covered_Sequences")

# def get_traced_events(folder_path):
#     return _get_specific_trace(folder_path, "Traced_events")




def main(argv):
    average_covered_sequences = {}
    average_traced_events = {}
    for experiment_path in experiments:
        average_covered_sequences[experiment_path] = {}
        average_traced_events[experiment_path] = {}

        covered_sequences_path_4 = {}
        traced_events_path_4 = {}
        for i in range(4):
            covered_sequences_path_4[i+1] = {}
            covered_sequences_path_4[i+1]["S1"] = list()
            covered_sequences_path_4[i+1]["S2"] = list()
            covered_sequences_path_4[i+1]["S3"] = list()
            traced_events_path_4[i+1] = {}
            traced_events_path_4[i+1]["S1"] = list()
            traced_events_path_4[i+1]["S2"] = list()
            traced_events_path_4[i+1]["S3"] = list()

            average_covered_sequences[experiment_path][i+1] = {}
            average_traced_events[experiment_path][i+1] = {}

        covered_sequences_path_3 = {}
        traced_events_path_3 = {}
        for i in range(3):
            covered_sequences_path_3[i+1] = {}
            covered_sequences_path_3[i+1]["S1"] = list()
            covered_sequences_path_3[i+1]["S2"] = list()
            covered_sequences_path_3[i+1]["S3"] = list()
            traced_events_path_3[i+1] = {}
            traced_events_path_3[i+1]["S1"] = list()
            traced_events_path_3[i+1]["S2"] = list()
            traced_events_path_3[i+1]["S3"] = list()
        for sub_folder in os.listdir(experiment_path):
            if sub_folder in services4:
                for i in range(4):
                    covered_sequences_path_4[i+1]["S1"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S1.trace_Covered_Sequences.log".format(sub_folder)))
                    covered_sequences_path_4[i+1]["S2"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S2.trace_Covered_Sequences.log".format(sub_folder)))
                    covered_sequences_path_4[i+1]["S3"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S3.trace_Covered_Sequences.log".format(sub_folder)))
                    
                    traced_events_path_4[i+1]["S1"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S1.trace_Traced_events.log".format(sub_folder)))
                    traced_events_path_4[i+1]["S2"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S2.trace_Traced_events.log".format(sub_folder)))
                    traced_events_path_4[i+1]["S3"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S3.trace_Traced_events.log".format(sub_folder)))
            
            elif sub_folder in services3:
                for i in range(3):
                    covered_sequences_path_3[i+1]["S1"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S1.trace_Covered_Sequences.log".format(sub_folder)))
                    covered_sequences_path_3[i+1]["S2"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S2.trace_Covered_Sequences.log".format(sub_folder)))
                    covered_sequences_path_3[i+1]["S3"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S3.trace_Covered_Sequences.log".format(sub_folder)))
                    
                    traced_events_path_3[i+1]["S1"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S1.trace_Traced_events.log".format(sub_folder)))
                    traced_events_path_3[i+1]["S2"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S2.trace_Traced_events.log".format(sub_folder)))
                    traced_events_path_3[i+1]["S3"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S3.trace_Traced_events.log".format(sub_folder)))

        for length, traces_logs in covered_sequences_path_4.items():  
            for trace_id, trace_log_list in traces_logs.items():
                if length in covered_sequences_path_3.keys():
                    trace_log_list.extend(covered_sequences_path_3[length][trace_id])
                np_logs = list()
                for trace_log in trace_log_list:
                    with open(trace_log, "rb") as f:
                        trace_log = pickle.load(f)
                    np_logs.append(np.array(trace_log))
                np_mean = np.mean(np_logs, axis=0)
                average_covered_sequences[experiment_path][length][trace_id] = np_mean

        for length, traces_logs in traced_events_path_4.items():  
            for trace_id, trace_log_list in traces_logs.items():
                if length in traced_events_path_3.keys():
                    trace_log_list.extend(traced_events_path_3[length][trace_id])
                np_logs = list()
                for trace_log in trace_log_list:
                    with open(trace_log, "rb") as f:
                        trace_log = pickle.load(f)
                    np_logs.append(np.array(trace_log))
                np_mean = np.mean(np_logs, axis=0)
                average_traced_events[experiment_path][length][trace_id] = np_mean
    print("Computation Done")

    print("Creating Covered Sequences Plots")
    labels = ["Lazy", "Medium", "Eager"]
    markers = ["o", "^", "s", "D"]
    for i in range(3):
        trace_id = "S" + str(i+1)
        for length in range(4):
            for j in range(len(experiments)):
                experiment_path = experiments[j]
                plt.plot(average_covered_sequences[experiment_path][length+1][trace_id], label=labels[j], marker=markers[j], markevery=1000000)
            plt.legend()
            plt.ticklabel_format(style="plain")
            plt.xlabel("Total Events")
            plt.ylabel("Covered Sequences")
            plt.ylim([0, 110])

            plt.savefig("Average_Covered_Sequences_{}_{}.pdf".format(str(length+1), trace_id), bbox_inches='tight', transparent="True", pad_inches=0.1)
            plt.close()
    print("Done for Covered Sequences")
    print("Creating Traced Events Plot")

    for i in range(3):
        trace_id = "S" + str(i+1)
        for length in range(4):
            for j in range(len(experiments)):
                experiment_path = experiments[j]
                plt.plot(average_traced_events[experiment_path][length+1][trace_id], label=labels[j], marker=markers[j], markevery=1000000)
            plt.legend()
            plt.ticklabel_format(style="plain")
            plt.xlabel("Total Events")
            plt.ylabel("Traced Events")
            #plt.ylim([0, 110])

            plt.savefig("Average_Traced_Events_{}_{}.pdf".format(str(length+1), trace_id), bbox_inches='tight', transparent="True", pad_inches=0.1)
            plt.close()







# def main(argv):
#     log_files = list()
#     output_name = argv[1]
#     y_label = argv[2]
#     for i in range(3, len(argv)):
#         log_files.append(argv[i])

#     #i = 2
#     #i = 100
#     i = 0
#     labels = ["Lazy", "Medium", "Eager"]
#     markers = ["o", "^", "s", "D"]
#     j = 0
#     for log_file in log_files:
#         with open(log_file, "rb") as f:
#             log_list = pickle.load(f)
#         #plt.plot(log_list, label="update rate = {}".format(i), marker=markers[j], markevery=1000000)
#         plt.plot(log_list, label=labels[i], marker=markers[i], markevery=1000000)
#         i += 1
#         #i *= 10
#         #j += 1
#     plt.legend()
#     plt.ticklabel_format(style="plain")
#     plt.xlabel("Total Events")
#     plt.ylabel(y_label)
#     plt.ylim([0, 110])

#     plt.savefig(output_name, bbox_inches='tight', transparent="True", pad_inches=0.1)
#     plt.close()
#     # with open("Merged_plot.log", "wb") as outfile:
#     #     pickle.dump(average.tolist(), outfile)

if __name__ == "__main__":
    main(sys.argv)
