import sys, pickle, os, json
from Tracer import Tracer
from itertools import tee
from tables import *

class Covered_sequences_count(IsDescription):
    step = Int32Col()
    count = Int32Col()

def window(iterable, size):
    iters = tee(iterable, size)
    for i in range(1, size):
        for each in iters[i:]:
            next(each, None)
    return zip(*iters)


def main(argv):

    trace_to_execute_path = argv[1]
    L_max = int(argv[2])

    print("Opening Trace: {}".format(trace_to_execute_path))
    with open(trace_to_execute_path, "r") as infile:
        trace_to_execute = json.load(infile)
    print("Trace loaded.")
    possible_sequences_folder_path = argv[3]

    print("Loading Sequences: {}".format(possible_sequences_folder_path))
    all_sequences, all_sequences_numbers, possible_requests, possible_responses = Tracer.load_sequences(possible_sequences_folder_path)
    tracer = Tracer(all_sequences, all_sequences_numbers, 0, 0, 1000, possible_requests, possible_responses)
    output_base_name = os.path.basename(trace_to_execute_path)
    output_base_name = output_base_name[:output_base_name.rindex(".")]
    print("Starting Analysis...")
    for i in range(L_max, L_max+1):
        covered_sequences = set()
        #covered_sequences_count = list()
        h5file = open_file("Perfect_tracing_{}_{}.h5".format(output_base_name, i), mode="w", title="Perfect Tracing")
        table = h5file.create_table('/', 'count', Covered_sequences_count, "Count")
        row = table.row
        step = 0
        for sequence in window(trace_to_execute, i):
            if tracer._find_sequence(sequence):
                covered_sequences.add(sequence)
            #covered_sequences_count.append(len(covered_sequences))
            row["step"] = step
            step+=1
            row["count"] = len(covered_sequences)
            row.append()

        for j in range(step, len(trace_to_execute)):
            #covered_sequences_count.append(len(covered_sequences))
            row["step"] = step
            step+=1
            row["count"] = len(covered_sequences)
            row.append()
        
        table.flush()        
        # with open("Perfect_tracing_{}_{}.log".format(output_base_name, i), "wb") as outfile:
        #     pickle.dump(covered_sequences_count, outfile, pickle.HIGHEST_PROTOCOL)
        # print(len(covered_sequences_count))
        print("sequences of lenght {} in trace: {}".format(i, len(covered_sequences)))
        h5file.close()
    
if __name__ == "__main__":
    main(sys.argv)