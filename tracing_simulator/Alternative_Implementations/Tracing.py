import sys, pickle, collections, time
import matplotlib.pyplot as plt

##
import random
##
class Tracing:

    def _init_sequences_from_pickle(self, sequences_pickle_path):
        with open(sequences_pickle_path, "rb") as f:
            self.all_sequences = pickle.load(f)

    def _init_probabilities(self):
        for key in self.all_sequences.keys():
            self.probabilities[key] = 1
        self.probabilities_log.append(self.probabilities.copy())

    def __init__(self, sequences_pickle_path, p_epsilon):
        start_time = time.time()
        print("Initialization started")
        self.p_epsilon = p_epsilon
        self.all_sequences = {}
        self.covered_sequences = collections.defaultdict(set)
        self.next_e = collections.defaultdict(set)
        self.probabilities={}
        
        self.probabilities_log = list()
        self.covered_sequences_number_log = list()
        self.traced_events_log = list()
        self.traced_events_log.append(0)

        self.events_buffer = None
        self.tracing = False
        self._init_sequences_from_pickle(sequences_pickle_path)
        self._init_probabilities()
        self.L = len(self.all_sequences[list(self.all_sequences.keys())[0]][0])
        self.update_rate = 100
        self.current_update_rate = 0
        self.M = self.L*2
        print("Initialization finished --- %s seconds ---" % (time.time() - start_time))



    
    def update_probabilities(self):
        #start_time = time.time()
        for e in self.covered_sequences.keys():
            self.update_probability(e)
        self.probabilities_log.append(self.probabilities.copy())
        #print("Update Probabilities --- %s seconds ---" % (time.time() - start_time))


    def update_probability(self, e):
        covered_sequences = len(self.covered_sequences[e])
        all_sequences = len(self.all_sequences[e])
        non_covered_sequences =  all_sequences - covered_sequences
        self.probabilities[e] = non_covered_sequences / all_sequences
    
    def _is_request(event):
        return event[0] == "0"

    def get_next_e(self, e):
        next_e = set()
        
        if self.L == 1 or len(list(self.covered_sequences.keys())) == 0:
            return next_e
            
        if self.L == 2:
            if Tracing._is_request(e): # REQUEST OPTIMIZATION
                e_sequences = self.covered_sequences[e]
                for e_sequence in e_sequences:
                    next_e.add(e_sequence[1])
            else: ## SEQUENCE BEGIN WITH REQ (LENGTH) OPTIMISATION
                return next_e
        else:
            for sequences_key in self.covered_sequences.keys():
                sequences_to_check = self.covered_sequences[sequences_key]
                for sequence_to_check in sequences_to_check:
                    for i in range(len(sequence_to_check)-1):
                        if sequence_to_check[i] == e:
                            next_e.add(sequence_to_check[i+1])
        return next_e
            
    def p_tracing(self, e_):
        next_e_ = self.next_e[e_]

        if len(next_e_) == 0: ### NO SEQUENCES COVERED OR SPECIAL CASES
            p_e = 1 
        else:
            p_e = 0
            for e in next_e_:
                if Tracing._is_request(e) and p_e < self.probabilities[e]:
                    p_e = self.probabilities[e]

        p_tracing = p_e*self.p_epsilon
        return p_tracing
    
    def add_to_covered_sequences(self, new_sequences):
        for new_sequence in new_sequences:
            self.covered_sequences[new_sequence[0]].add(new_sequence)
            for i in range(len(new_sequence)-1):
                self.next_e[new_sequence[i]].add(new_sequence[i+1])
        
    def start_tracing(self):
        self.events_buffer = tuple()
        self.tracing = True

    def _get_possible_sequences(self):
        possible_sequences = set()
        for i in range(self.M-self.L+1):
            possible_sequence = tuple()
            possible_sequence += self.events_buffer[i:self.L+i]
            if Tracing._is_request(possible_sequence[0]):
                if possible_sequence in self.all_sequences[possible_sequence[0]]:
                    possible_sequences.add(possible_sequence)
        return possible_sequences
                

    def stop_tracing(self):
        self.tracing = False
        possible_sequences = self._get_possible_sequences()
        
        self.add_to_covered_sequences(possible_sequences)
        self.events_buffer = None

    def read_event(self, event):
        if self.tracing:
            self.traced_events_log.append(self.traced_events_log[-1]+1)
            self.events_buffer += (event,) ## CONTROLLARE SE E' GIA' UNA TUPLA
            if len(self.events_buffer) == self.M:
                self.stop_tracing()
        else:
            random_prob = random.random()
            p_tracing = self.p_tracing(event)
            if p_tracing > random_prob:
                self.start_tracing()
        
        self.check_probabilities_update()
        self.covered_sequences_number_log.append(sum([len(self.covered_sequences[x]) for x in self.covered_sequences.keys()]))
        
        
    def check_probabilities_update(self):
        self.current_update_rate += 1
        if self.current_update_rate == self.update_rate:
            self.update_probabilities()
            self.current_update_rate = 0


########


def plot(t, file_prefix):
    plt.plot(t.covered_sequences_number_log)
    plt.xlabel("Total Events")
    plt.ylabel("Covered Sequences")
    plt.savefig(file_prefix + "Covered_Sequences.pdf")
    plt.close()
    plt.plot(t.traced_events_log)
    plt.xlabel("Total Events")
    plt.ylabel("Number of traced events")
    plt.savefig(file_prefix + "Traced_Events.pdf")
    


def main(argv):
    tp = Tracing(argv[1], 0.2)
    start_time = time.time()

    for i in range(100000*tp.L):
        random_key_index = random.randint(0, len(tp.all_sequences.keys())-1)
        random_key = list(tp.all_sequences.keys())[random_key_index]
        random_sequence_index = random.randint(0,len(tp.all_sequences[random_key])-1)
        random_sequence = tp.all_sequences[random_key][random_sequence_index]
        random_event_index = random.randint(0,len(random_sequence)-1)
        random_event = random_sequence[random_event_index]
        tp.read_event(random_event)
    print("Process finished --- %s seconds ---" % (time.time() - start_time))
    plot(tp, argv[1])




if __name__ == "__main__":
    main(sys.argv)