from os import wait
import sys
import json
import collections
from typing import NewType, Sequence
import itertools
import time
import pickle
import bz2
import psutil
import gc
from threading import Thread
from itertools import islice
import multiprocessing



class Sequence_Generator:

    def _join_tuple(self, tuple):
        return "".join(map(str, tuple))

    def _join_tuples(self, tuples_list):
        to_ret = set()
        for tuple in tuples_list:
            to_ret.add(self._join_tuple(tuple))
        return to_ret

    def _get_tuples_iterator(self, input_list, k=1):
        if all(isinstance(item, list) for item in input_list):
            # in questo caso dovrebbero essere TUTTE liste
            tuples = itertools.product(*input_list, repeat=k)
        else:
            tuples = itertools.product(input_list, repeat=k)
        return tuples

    def _get_list_product(self, input_list, k=1):
        tuples = self._get_tuples_iterator(input_list, k)
        tuples_list = [x for x in tuples]
        return tuples_list

    def _get_list_product_as_strings(self, input_list, k=1):
        return self._join_tuples(self._get_list_product(input_list=input_list, k=k))

    def _get_combinations(self, sequence_template):
        exploded_list = list()
        for item in sequence_template:
            if isinstance(item, list):
                exploded_list.append(item)
            if isinstance(item, str):
                exploded_item = self.custom_objects[item]
                if any(isinstance(exploded_item_sub, list) for exploded_item_sub in exploded_item):
                    exploded_list.append(
                        list(self._get_list_product_as_strings(exploded_item)))
                else:
                    exploded_list.append(exploded_item)

        if len(exploded_list) > 1:
            combinations = self._get_list_product_as_strings(exploded_list)
        else:
            combinations = exploded_list[0]
        return combinations

    def __get_res_req(self, req_res_list, prefix):
        to_ret = list()
        for res_req in req_res_list:
            if req_res_list[res_req]:
                combinations = self._get_combinations(req_res_list[res_req])
                combinations = [prefix + res_req + str(sub) for sub in combinations]
                to_ret.extend(combinations)
            else:
                to_ret.append(prefix + res_req)
        return to_ret

    def _init_possible_requests(self):
        self.possible_requests = self.__get_res_req(
            self.requests_template, self.requests_prefix)

    def _init_possibile_responses(self):
        self.possible_responses = self.__get_res_req(
            self.responses_template, self.responses_prefix)
        self.__filter_responses()

    def __filter_responses(self):
        # third char in every response req_prefix,endpoint,status_code,payload
        status_code_index = 2
        bad_responses = [x for x in self.possible_responses if x[status_code_index] != "1"]
        self.possible_responses = [x for x in self.possible_responses if x not in bad_responses]
        new_bad_responses = [x[:status_code_index + 1] for x in bad_responses]
        new_bad_responses = list(dict.fromkeys(new_bad_responses))

        if hasattr(self,'nullable_responses'):
            good_responses = [x[:status_code_index + 1] for x in self.possible_responses]

            good_responses = list(dict.fromkeys(good_responses))
            #new_good_responses = [x for x in good_responses if self.nullable_responses[x[1][0]] == 1]
            new_good_responses = list()
            for gr in good_responses:
                index = gr[1]
                if self.nullable_responses[index][0] == 1:
                    new_good_responses.append(gr)
            self.possible_responses.extend(new_good_responses)
        
        self.possible_responses.extend(new_bad_responses)

    def __init__(self, service_file_path, service_name):
        with open(service_file_path) as json_file:
            data = json.load(json_file)
        data = data[service_name]
        self.custom_objects = data["custom_objects"]
        self.requests_template = data["requests"]
        self.responses_template = data["responses"]
        self.requests_prefix = data["requests_prefix"]
        self.responses_prefix = data["responses_prefix"]
        if "nullable_responses" in data:
            self.nullable_responses = data["nullable_responses"]
        self._init_possible_requests()
        self._init_possibile_responses()

    def __validate_sequence(self, tuple):
        allowed_responses = list()
        for tuple_item in tuple:
            if tuple_item[0] is self.requests_prefix:
                allowed_responses.append(tuple_item[1])
            elif tuple_item[1] in allowed_responses:
                allowed_responses.remove(tuple_item[1])
            else:
                return False
        return True

    def _thread_validate_sequences(self, sequences, results):
        need_to_stop = False
        sequences_numbers = {}
        for sequence in sequences:
            first = sequence[0][0]
            if first == "1":
                need_to_stop = True
                break
            if self.__validate_sequence(sequence):
                if not sequence[0] in sequences_numbers.keys():
                    sequences_numbers[sequence[0]] = 1
                else:
                    sequences_numbers[sequence[0]] += 1
        results.append((need_to_stop, sequences_numbers))

    def _generate_sequences_iterator(self, length):
        all_items_list = list()
        all_items_list.extend(self.possible_requests)
        all_items_list.extend(self.possible_responses)
        tuples_iterator = self._get_tuples_iterator(all_items_list, length)
        #valid_tuples = filter(self.__validate_sequence, tuples_iterator)
        return tuples_iterator

    def _get_sequences(self, sequences_iterator, slices, slice_size, finished):
        for x in iter(lambda: list(itertools.islice(sequences_iterator, slice_size)), []):
            slices.append(x)
        finished.set(True)

    def _write_sequences(self, sequences_iterator, file_name):
        
        sequences_numbers = {}
        total_sequences = 0
        manager = multiprocessing.Manager()

        slice_size = 10000000
        results = manager.list()
        threads=[None] * 28
        finished = False
        while True:
            for i in range(len(threads)):
                slice = self._get_slice(sequences_iterator, slice_size)
                if len(slice) == 0:
                    finished = True
                    break
                else:
                    threads[i] = multiprocessing.Process(target=self._thread_validate_sequences, args=(slice, results))
                    threads[i].start()

            for i in range(len(threads)):
                if threads[i]:
                    threads[i].join()
                    threads[i] = None
                
            no_more_requests, total_sequences, sequences_numbers = self.__update_sequences(total_sequences, sequences_numbers, results)
            if no_more_requests or finished:
                break
        self.__write_files(sequences_numbers, total_sequences, file_name)
        print("Generation Done.")
        return sequences_numbers

    def _get_slice(self, sequences_iterator, slice_size):
        return list(itertools.islice(sequences_iterator, slice_size))
    # #     yield x
    # # yield None

    # def _get_slices(self, sequences_iterator, slice_size, slices):
    #     while True:
    #         slice = self._get_slice(sequences_iterator, slice_size)
    #         if len(slice) > 0:
    #             slices.put(slice)
    #         else:
    #             slices.put(None)
    #             break

    # def _write_sequences(self, sequences_iterator, file_name):
        
    #     sequences_numbers = {}
    #     total_sequences = 0
    #     manager = multiprocessing.Manager()
    #     slice_size = 10000000
    #     results = manager.list()
    #     threads=[None] * 28
    #     slices = manager.Queue()
    #     producer = multiprocessing.Process(target=self._get_slices, args=(sequences_iterator, slice_size, slices))
    #     producer.start()
    #     running_threads = 0
    #     while True:
    #         for i in range(len(threads)):
    #             slice = slices.get()
    #             if slice is None:
    #                 break
    #             else:
    #                 threads[i] = multiprocessing.Process(target=self._thread_validate_sequences, args=(slice, results))
    #                 threads[i].start()
    #                 running_threads += 1

    #         for i in range(len(threads)):
    #             if threads[i]:
    #                 threads[i].join()
    #                 threads[i] = None
    #                 running_threads -= 1
    #         no_more_requests, total_sequences, sequences_numbers = self.__update_sequences(total_sequences, sequences_numbers, results)

    #         if no_more_requests and len(results) == 0 and running_threads == 0:
    #             break
    #     self.__write_files(sequences_numbers, total_sequences, file_name)
    #     print("Generation Done.")
    #     return sequences_numbers

    def __update_sequences(self,total_sequences, sequences_numbers, results):
        no_more_requests = False
        for tuple in results:
            results.remove(tuple)
            no_more_requests = no_more_requests or tuple[0]
            local_results = tuple[1]
            for key in local_results.keys():
                total_sequences += local_results[key]
                # chunk += local_results[key]

                if key in sequences_numbers.keys():
                    sequences_numbers[key] += local_results[key]
                else:
                    sequences_numbers[key] = local_results[key]
        return no_more_requests, total_sequences, sequences_numbers
    

    def __write_files(self, sequences_numbers, total_sequences, file_name):
        print("Double checking the number of sequences")
        checksum = sum(sequences_numbers.values())
        if checksum == total_sequences:
            print("CHECKSUM OK")
            sequences_numbers["total"] = total_sequences
        else:
            print("CHECKSUM KO: ERROR")

        print("Writing support file to disk")
        with open("{}_support.p".format(file_name), 'wb') as f:
            pickle.dump(sequences_numbers, f)

    def generate_sequences(self, length, file_name):
        sequences_iterator = self._generate_sequences_iterator(length)
        sequences_numbers = self._write_sequences(sequences_iterator, file_name)
        return sequences_numbers



def main(argv):
    print("Service: {}".format(argv[2]))
    print("Length: {}".format(argv[3]))
    generator = Sequence_Generator(argv[1], argv[2])
    print("Possible Requests: {}".format(len(generator.possible_requests)))
    print("Admissible Responses: {}".format(len(generator.possible_responses)))
    max_length = int(argv[3])
    file_name = "{}_{}".format(argv[2], max_length)
    with open("{}_{}_req_res.p".format(argv[2], max_length), 'wb') as f:
        pickle.dump((generator.possible_requests, generator.possible_responses), f)

    start_time = time.time()
    
    sequences_total = generator.generate_sequences(int(max_length), file_name)
    # with open("{}_{}.p".format(argv[2], max_length), 'wb') as f:
    #     pickle.dump(sequences_map, f)
    # with open("{}_{}_support.p".format(argv[2], max_length), 'wb') as f:
    #     pickle.dump(sequences_total, f)
    print("Total number of admissible sequences of length {}: {}".format(
        max_length, sequences_total["total"]))
    print("Process finished --- %s seconds ---" %
            (time.time() - start_time))


if __name__ == "__main__":
    main(sys.argv)
