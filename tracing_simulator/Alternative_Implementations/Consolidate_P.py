import pickle, bz2,os, sys, gc
from os import listdir
from os.path import isfile, join


def add_to_map(sequences, map_to_add):
    to_update = sequences
    for key in map_to_add.keys():
        if not key in to_update.keys():
            to_update[key] = map_to_add[key]
        else:
            add_to_map(to_update[key], map_to_add[key])


sequences_pickle_folder_path = sys.argv[1]
pickle_files_list = [os.path.join(sequences_pickle_folder_path, f) for f in listdir(sequences_pickle_folder_path) if isfile(join(sequences_pickle_folder_path, f)) and f.endswith(".p")]
sequences_files={}
for pickle_file_path in pickle_files_list:
    if "support" in pickle_file_path or "req_res" in pickle_file_path:
        continue
    base= os.path.basename(pickle_file_path)[0:os.path.basename(pickle_file_path).index('_')]
    if not base in sequences_files.keys():
        sequences_files[base] = list()        
    sequences_files[base].append(pickle_file_path)

for id in sequences_files.keys():
    sequences = {}
    if len(sequences_files[id]) == 1:
        continue
    for file_path in sequences_files[id]:
        with bz2.BZ2File(file_path, "rb") as f:
            pickle_obj = pickle.load(f)
            add_to_map(sequences, pickle_obj)
            os.remove(file_path)
                    
    with bz2.BZ2File("{}_1.p".format(id), 'wb') as f: 
        pickle.dump(sequences, f)
    del sequences
    gc.collect()
    
