#!/bin/bash

wd=/media/kralizec/Data2/JSEP_Simulator

aggressive="experiment_run_0.05_0.2_1000"
moderate="experiment_run_0.03_0.1_1000"
conservative="experiment_run_0.01_0.05_1000"

y_label1="Covered Sequences (%)"
y_label2="Traced Events"

services4="notification_service statistics_service ts_food_map_service account_service"
services3="ts_contacts_service ts_consign_price_service"
#services4=""

command=$wd/merge_plots.py

seq_length4="1 2 3 4"
seq_length3="1 2 3"

for service in $services4
do
    for i in $seq_length4
    do
        for trace in {1..3}
        do
            # file_name="${service}_${i}_S${trace}_Covered_Sequences.pdf" 
            # python3 $command $file_name "$y_label1" "${conservative}/${service}/${i}/${service}_S${trace}.trace_Covered_Sequences.log" "$moderate/${service}/${i}/${service}_S${trace}.trace_Covered_Sequences.log" "$aggressive/${service}/${i}/${service}_S${trace}.trace_Covered_Sequences.log" &
            
            file_name2="${service}_${i}_S${trace}_Traced_Events.pdf"
            python3 $command $file_name2 "$y_label2" "${conservative}/${service}/${i}/${service}_S${trace}.trace_Traced_events.log" "$moderate/${service}/${i}/${service}_S${trace}.trace_Traced_events.log" "$aggressive/${service}/${i}/${service}_S${trace}.trace_Traced_events.log" &
            #wait
        done
    done
done
wait
for service in $services3
do
    for i in $seq_length3
    do
        for trace in {1..3}
        do
            # file_name="${service}_${i}_${i}_S${trace}_Covered_sequences.pdf" 
            # python3 $command $file_name "$y_label1" "${conservative}/${service}/${i}/${service}_S${trace}.trace_Covered_Sequences.log" "$moderate/${service}/${i}/${service}_S${trace}.trace_Covered_Sequences.log" "$aggressive/${service}/${i}/${service}_S${trace}.trace_Covered_Sequences.log" &

            file_name2="${service}_${i}_S${trace}_Traced_Events.pdf"
            python3 $command $file_name2 "$y_label2" "${conservative}/${service}/${i}/${service}_S${trace}.trace_Traced_events.log" "$moderate/${service}/${i}/${service}_S${trace}.trace_Traced_events.log" "$aggressive/${service}/${i}/${service}_S${trace}.trace_Traced_events.log" &
        done
    done
done
wait
