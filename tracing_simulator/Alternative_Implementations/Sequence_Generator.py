import random
import sys
import json
import collections
from typing import NewType, Sequence
import itertools
import time
import pickle
import bz2
import psutil
import gc


class Sequence_Generator:

    def _join_tuple(self, tuple):
        return "".join(map(str, tuple))

    def _join_tuples(self, tuples_list):
        to_ret = set()
        for tuple in tuples_list:
            to_ret.add(self._join_tuple(tuple))
        return to_ret

    def _get_tuples_iterator(self, input_list, k=1):
        if all(isinstance(item, list) for item in input_list):
            # in questo caso dovrebbero essere TUTTE liste
            tuples = itertools.product(*input_list, repeat=k)
        else:
            tuples = itertools.product(input_list, repeat=k)
        return tuples

    def _get_list_product(self, input_list, k=1):
        tuples = self._get_tuples_iterator(input_list, k)
        tuples_list = [x for x in tuples]
        return tuples_list

    def _get_list_product_as_strings(self, input_list, k=1):
        return self._join_tuples(self._get_list_product(input_list=input_list, k=k))

    def _get_combinations(self, sequence_template):
        exploded_list = list()
        for item in sequence_template:
            if isinstance(item, list):
                exploded_list.append(item)
            if isinstance(item, str):
                exploded_item = self.custom_objects[item]
                if any(isinstance(exploded_item_sub, list) for exploded_item_sub in exploded_item):
                    exploded_list.append(
                        list(self._get_list_product_as_strings(exploded_item)))
                else:
                    exploded_list.append(exploded_item)

        if len(exploded_list) > 1:
            combinations = self._get_list_product_as_strings(exploded_list)
        else:
            combinations = exploded_list[0]
        return combinations

    def __get_res_req(self, req_res_list, prefix):
        to_ret = list()
        for res_req in req_res_list:
            if req_res_list[res_req]:
                combinations = self._get_combinations(req_res_list[res_req])
                combinations = [prefix + res_req + str(sub) for sub in combinations]
                to_ret.extend(combinations)
            else:
                to_ret.append(prefix + res_req)
        return to_ret

    def _init_possible_requests(self):
        self.possible_requests = self.__get_res_req(
            self.requests_template, self.requests_prefix)

    def _init_possibile_responses(self):
        self.possible_responses = self.__get_res_req(
            self.responses_template, self.responses_prefix)
        self.__filter_responses()

    def __filter_responses(self):
        # third char in every response req_prefix,endpoint,status_code,payload
        status_code_index = 2
        bad_responses = [x for x in self.possible_responses if x[status_code_index] != "1"]
        self.possible_responses = [x for x in self.possible_responses if x not in bad_responses]
        new_bad_responses = [x[:status_code_index + 1] for x in bad_responses]
        new_bad_responses = list(dict.fromkeys(new_bad_responses))

        if hasattr(self,'nullable_responses'):
            good_responses = [x[:status_code_index + 1] for x in self.possible_responses]

            good_responses = list(dict.fromkeys(good_responses))
            #new_good_responses = [x for x in good_responses if self.nullable_responses[x[1][0]] == 1]
            new_good_responses = list()
            for gr in good_responses:
                index = gr[1]
                if self.nullable_responses[index][0] == 1:
                    new_good_responses.append(gr)
            self.possible_responses.extend(new_good_responses)
        
        self.possible_responses.extend(new_bad_responses)

    def __init__(self, service_file_path, service_name):
        with open(service_file_path) as json_file:
            data = json.load(json_file)
        data = data[service_name]
        self.custom_objects = data["custom_objects"]
        self.requests_template = data["requests"]
        self.responses_template = data["responses"]
        self.requests_prefix = data["requests_prefix"]
        self.responses_prefix = data["responses_prefix"]
        if "nullable_responses" in data:
            self.nullable_responses = data["nullable_responses"]
        self._init_possible_requests()
        self._init_possibile_responses()

    def __validate_sequence(self, tuple):
        allowed_responses = list()
        for tuple_item in tuple:
            if tuple_item[0] is self.requests_prefix:
                allowed_responses.append(tuple_item[1])
            elif tuple_item[1] in allowed_responses:
                allowed_responses.remove(tuple_item[1])
            else:
                return False
        return True

    def _get_random_request(self):
        return random.choice(self.possible_requests)
    def _get_random_response(self):
        return random.choice(self.possible_responses)

    def get_random_valid_sequence(self, length):
        sequence = list()
        allowed_responses = list()
        
        #BOOTSTRAP WITH A REQUEST
        first = self._get_random_request()
        sequence.append(first)
        allowed_responses.append(first[1])

        for i in range(length-1):
            if random.randint(0,1) == 0: #REQUEST
                req = self._get_random_request()
                sequence.append(req)
                allowed_responses.append(req[1])
            else:
                ok_res = False
                while not ok_res:
                    res = self._get_random_response()
                    if res[1] in allowed_responses:
                        allowed_responses.remove(res[1])
                        sequence.append(res)
                        ok_res = True
        return tuple(sequence)

    def _s_validate_sequence(tuple):
        allowed_responses = list()
        for tuple_item in tuple:
            if tuple_item[0] == "0":
                allowed_responses.append(tuple_item[1])
            elif tuple_item[1] in allowed_responses:
                allowed_responses.remove(tuple_item[1])
            else:
                return False
        return True

    def _generate_sequences_iterator(self, length):
        all_items_list = list()
        all_items_list.extend(self.possible_requests)
        all_items_list.extend(self.possible_responses)
        tuples_iterator = self._get_tuples_iterator(all_items_list, length)
        #valid_tuples = filter(self.__validate_sequence, tuples_iterator)
        return tuples_iterator

    def _write_sequences_iter_map(self, sequences_iterator, file_name):
        to_ret_map = {}
        sequences_numbers = {}
        total_sequences = 0
        sequence_begin = ""
        chunk = 1
        
        for sequence in sequences_iterator:
            if sequence[0][0] == "1":
                break
            if not self.__validate_sequence(sequence):
                continue
            if sequence_begin == sequence[0]:
                sequences_numbers[sequence[0]] += 1
            else:
                sequences_numbers[sequence[0]] = 1
                sequence_begin = sequence[0]
            if total_sequences % 100000 == 0:
                if psutil.virtual_memory().percent > 70:
                    print("Writing chunk number {} to disk".format(chunk))
                    for key in to_ret_map.keys():
                        with bz2.BZ2File("{}_{}.p".format(key, chunk), 'wb') as f: 
                            to_write = {key:to_ret_map[key]}
                            pickle.dump(to_write, f)
                    del to_ret_map
                    gc.collect()
                    chunk += 1
                    to_ret_map = {}
                print("Sequences Created: {}".format(total_sequences))
            total_sequences +=1
            temp_map = to_ret_map
            for tuple in sequence:
                if not tuple in temp_map.keys():
                        temp_map[tuple] = {}
                temp_map = temp_map[tuple]
        
        print("Double checking the number of sequences")
        checksum = sum(sequences_numbers.values())
        if checksum == total_sequences:
            print("CHECKSUM OK")
            sequences_numbers["total"] = total_sequences
        else:
            print("CHECKSUM KO: ERROR")

        print("Writing chunk number {} to disk".format(chunk))
        for key in to_ret_map.keys():
            with bz2.BZ2File("{}_{}.p".format(key, chunk), 'wb') as f: 
                to_write = {key:to_ret_map[key]}
                pickle.dump(to_write, f)

        print("Writing support file to disk")
        with open("{}_support.p".format(file_name), 'wb') as f:
            pickle.dump(sequences_numbers, f)
        print("Generation Done.")
        return sequences_numbers

    def generate_sequences(self, length, file_name):
        sequences_iterator = self._generate_sequences_iterator(length)
        sequences_numbers = self._write_sequences_iter_map(sequences_iterator, file_name)

        return sequences_numbers



def main(argv):
    start_time = time.time()
    print("Service: {}".format(argv[2]))
    print("Length: {}".format(argv[3]))
    
    generator = Sequence_Generator(argv[1], argv[2])
    print("Possible Requests: {}".format(len(generator.possible_requests)))
    print("Admissible Responses: {}".format(len(generator.possible_responses)))
    max_length = int(argv[3])
    file_name = "{}_{}".format(argv[2], max_length)
    with open("{}_{}_req_res.p".format(argv[2], max_length), 'wb') as f:
        pickle.dump((generator.possible_requests, generator.possible_responses), f)

    
    
    #sequences_total = generator.generate_sequences(int(max_length), file_name)
    # with open("{}_{}.p".format(argv[2], max_length), 'wb') as f:
    #     pickle.dump(sequences_map, f)
    # with open("{}_{}_support.p".format(argv[2], max_length), 'wb') as f:
    #     pickle.dump(sequences_total, f)
    # print("Total number of admissible sequences of length {}: {}".format(
    #     max_length, sequences_total["total"]))
    print("Process finished --- %s seconds ---" %
            (time.time() - start_time))


if __name__ == "__main__":
    main(sys.argv)
