#!/bin/sh

scenarios_folder=$wd/scenarios/$service
service_folder=$wd/$service
sequences2=$service_folder/2

traces_folder=$wd/traces/$service
rm -r $traces_folder
mkdir -p $traces_folder
cd $traces_folder
python3 $wd/Traces_Creator.py $services_file $service $scenarios_folder $trace_length > "${service}_trace_generator.log" 2>&1