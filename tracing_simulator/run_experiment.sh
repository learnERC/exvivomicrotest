#!/bin/bash

experiment_folder=$wd/experiment_run_$p_min\_$p_max\_$update_rate/$service

service_perfect_tracing_folder=$perfect_tracing_folder/$service


for trace_index in 1 2 3
do
    service_trace="${traces_folder}/$service/${service}_S${trace_index}.trace"
    
    for length in $seq_length
    do
        run_folder=$experiment_folder/$length/
        mkdir -p $run_folder
        cd $run_folder

        service_perfect_tracing="${service_perfect_tracing_folder}/Perfect_tracing_${service}_S${trace_index}_${length}.p"
        service_sequences=$wd/$service/$length

        python3 $command $service_trace $service_perfect_tracing $number_of_runs $service_sequences $length $p_min $p_max $update_rate $run_for > "${service}_S${trace_index}_${length}_${p_min}_${p_max}_${update_rate}.log" 2>&1 &
    done
    wait
done