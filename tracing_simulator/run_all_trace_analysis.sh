#!/bin/bash

wd=$pwd

services4="notification_service statistics_service ts_food_map_service account_service"
services3="ts_contacts_service ts_consign_price_service"

seq_length4="1 2 3 4"
seq_length3="1 2 3"
for service in $services4
do
    seq_length=$seq_length4
    . $wd/run_trace_analysis.sh &
done
for service in $services3
do
    seq_length=$seq_length3
    . $wd/run_trace_analysis.sh &
done
wait