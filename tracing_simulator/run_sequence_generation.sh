#!/bin/bash

service_folder=$wd/$service
rm -r $service_folder
mkdir $service_folder

for i in $seq_length;
do
    cd $wd
    run_folder=$service_folder/$i
    mkdir $run_folder
    cd $run_folder
    python3 $command $services_file $service $i > "${service}_${i}_sequence_generator.log" 2>&1
done
