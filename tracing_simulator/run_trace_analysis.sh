#!/bin/sh

service_folder=$wd/$service
traces_folder=$wd/traces

run_folder=perfect_tracing/$service
rm -r $run_folder
mkdir -p $run_folder
cd $run_folder

for scenario in 1 2 3
do
    for length in $seq_length
    do
        sequences_folder=$service_folder/$length
        trace=$traces_folder/$service/"${service}_S${scenario}.trace"
        python3 $wd/Trace_Analyser.py $trace $length $sequences_folder > "${service}_${scenario}_${length}_trace_analysis.log" 2>&1
    done
done
