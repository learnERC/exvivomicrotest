import numpy as np

class Tracing_Profile:

    def __init__(self, p_tracing_map):        
        self._p_tracing_map = p_tracing_map
        self.p_tracing_map_mean_value = np.mean(list(self.p_tracing_map.values()))

    @property
    def p_tracing_map(self):
        return self._p_tracing_map

    @p_tracing_map.setter
    def p_tracing_map(self, value):
        self._p_tracing_map = value
        self.p_tracing_map_mean_value = np.mean(list(self.p_tracing_map.values()))


    


