#!/usr/bin/env python3
import os, sys, pickle
import matplotlib.pyplot as plt
import numpy as np

services4 = ["notification_service", "statistics_service", "ts_food_map_service", "account_service"]
services3 = ["ts_contacts_service", "ts_consign_price_service"]

lazy_path = "/home/exvivomicrotest/tracing_simulator/experiment_run_0.01_0.05_1000"
medium_path = "/home/exvivomicrotest/tracing_simulator/experiment_run_0.03_0.1_1000"
eager_path = "/home/exvivomicrotest/tracing_simulator/experiment_run_0.05_0.2_1000"

curve =["notification_service", "statistics_service", "ts_food_map_service"]
flat = ["ts_contacts_service", "ts_consign_price_service", "account_service"]

experiments = [lazy_path, medium_path, eager_path]

def main(argv):
    average_covered_sequences = {}
    average_traced_events = {}
    for experiment_path in experiments:
        average_covered_sequences[experiment_path] = {}
        average_traced_events[experiment_path] = {}

        covered_sequences_path_4 = {}
        traced_events_path_4 = {}
        for i in range(4):
            covered_sequences_path_4[i+1] = {}
            covered_sequences_path_4[i+1]["S1"] = list()
            covered_sequences_path_4[i+1]["S2"] = list()
            covered_sequences_path_4[i+1]["S3"] = list()
            traced_events_path_4[i+1] = {}
            traced_events_path_4[i+1]["S1"] = list()
            traced_events_path_4[i+1]["S2"] = list()
            traced_events_path_4[i+1]["S3"] = list()

            average_covered_sequences[experiment_path][i+1] = {}
            average_traced_events[experiment_path][i+1] = {}

        covered_sequences_path_3 = {}
        traced_events_path_3 = {}
        for i in range(3):
            covered_sequences_path_3[i+1] = {}
            covered_sequences_path_3[i+1]["S1"] = list()
            covered_sequences_path_3[i+1]["S2"] = list()
            covered_sequences_path_3[i+1]["S3"] = list()
            traced_events_path_3[i+1] = {}
            traced_events_path_3[i+1]["S1"] = list()
            traced_events_path_3[i+1]["S2"] = list()
            traced_events_path_3[i+1]["S3"] = list()
        for sub_folder in os.listdir(experiment_path):
            if sub_folder in services4:
                for i in range(4):
                    covered_sequences_path_4[i+1]["S1"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S1.trace_Covered_Sequences.log".format(sub_folder)))
                    covered_sequences_path_4[i+1]["S2"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S2.trace_Covered_Sequences.log".format(sub_folder)))
                    covered_sequences_path_4[i+1]["S3"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S3.trace_Covered_Sequences.log".format(sub_folder)))
                    
                    traced_events_path_4[i+1]["S1"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S1.trace_Traced_events.log".format(sub_folder)))
                    traced_events_path_4[i+1]["S2"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S2.trace_Traced_events.log".format(sub_folder)))
                    traced_events_path_4[i+1]["S3"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S3.trace_Traced_events.log".format(sub_folder)))
            
            elif sub_folder in services3:
                for i in range(3):
                    covered_sequences_path_3[i+1]["S1"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S1.trace_Covered_Sequences.log".format(sub_folder)))
                    covered_sequences_path_3[i+1]["S2"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S2.trace_Covered_Sequences.log".format(sub_folder)))
                    covered_sequences_path_3[i+1]["S3"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S3.trace_Covered_Sequences.log".format(sub_folder)))
                    
                    traced_events_path_3[i+1]["S1"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S1.trace_Traced_events.log".format(sub_folder)))
                    traced_events_path_3[i+1]["S2"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S2.trace_Traced_events.log".format(sub_folder)))
                    traced_events_path_3[i+1]["S3"].append(os.path.join(experiment_path, sub_folder, str(i+1), "{}_S3.trace_Traced_events.log".format(sub_folder)))

        for length, traces_logs in covered_sequences_path_4.items():  
            for trace_id, trace_log_list in traces_logs.items():
                if length in covered_sequences_path_3.keys():
                    trace_log_list.extend(covered_sequences_path_3[length][trace_id])
                np_logs = list()
                for trace_log in trace_log_list:
                    with open(trace_log, "rb") as f:
                        trace_log = pickle.load(f)
                    np_logs.append(np.array(trace_log))
                np_mean = np.mean(np_logs, axis=0)
                average_covered_sequences[experiment_path][length][trace_id] = np_mean

        for length, traces_logs in traced_events_path_4.items():  
            for trace_id, trace_log_list in traces_logs.items():
                if length in traced_events_path_3.keys():
                    trace_log_list.extend(traced_events_path_3[length][trace_id])
                np_logs_flat = list()
                np_logs_curve = list()
                average_traced_events[experiment_path][length][trace_id] = {}
                for trace_log_path in trace_log_list:
                    with open(trace_log_path, "rb") as f:
                        trace_log = pickle.load(f)
                    if (any(x in trace_log_path for x in flat)):
                        np_logs_flat.append(np.array(trace_log))
                    if (any(x in trace_log_path for x in curve)):
                        np_logs_curve.append(np.array(trace_log))
                np_mean_flat = np.mean(np_logs_flat, axis=0)
                np_mean_curve = np.mean(np_logs_curve, axis=0)
                average_traced_events[experiment_path][length][trace_id]["flat"] = np_mean_flat
                average_traced_events[experiment_path][length][trace_id]["curve"] = np_mean_curve
    print("Computation Done")

    labels = ["Lazy", "Medium", "Eager"]
    markers = ["o", "^", "s", "D"]
    print("Creating Covered Sequences Plots")
    for i in range(3):
        trace_id = "S" + str(i+1)
        for length in range(4):
            for j in range(len(experiments)):
                experiment_path = experiments[j]
                plt.plot(average_covered_sequences[experiment_path][length+1][trace_id], label=labels[j], marker=markers[j], markevery=1000000)
            plt.legend()
            plt.ticklabel_format(style="plain")
            plt.xlabel("Total Events")
            plt.ylabel("Covered Sequences")
            plt.ylim([0, 110])

            plt.savefig("Average_Covered_Sequences_{}_{}.pdf".format(str(length+1), trace_id), bbox_inches='tight', transparent="True", pad_inches=0.1)
            plt.close()
    print("Done for Covered Sequences")
    print("Creating Traced Events Plot")

    for i in range(3):
        trace_id = "S" + str(i+1)
        for length in range(4):
            fig, (ax1, ax2) = plt.subplots(1, 2)
            for j in range(len(experiments)):
                experiment_path = experiments[j]
                ax1.plot(average_traced_events[experiment_path][length+1][trace_id]["curve"], label=labels[j], marker=markers[j], markevery=1000000)
                ax2.plot(average_traced_events[experiment_path][length+1][trace_id]["flat"], label=labels[j], marker=markers[j], markevery=1000000)
            ax1.legend()
            ax2.legend()
            ax1.set_xlabel("Total Events")
            ax2.set_xlabel("Total Events")
            ax1.set_ylabel("Traced Events")
            ax2.set_ylabel("Traced Events")
            plt.tight_layout() # Or equivalently,  "plt.tight_layout()"                
            plt.savefig("Average_Traced_Events_{}_{}_flat_curve.pdf".format(str(length+1), trace_id), bbox_inches='tight', transparent="True", pad_inches=0.1)
            plt.close()

    for i in range(3):
        trace_id = "S" + str(i+1)
        for length in range(4):
            for j in range(len(experiments)):
                experiment_path = experiments[j]
                plt.plot(average_traced_events[experiment_path][length+1][trace_id]["curve"], label=labels[j], marker=markers[j], markevery=1000000)
            plt.legend()
            plt.ticklabel_format(style="plain")
            plt.xlabel("Total Events")
            plt.ylabel("Traced Events")
            #plt.ylim([0, 110])

            plt.savefig("Average_Traced_Events_{}_{}_curve.pdf".format(str(length+1), trace_id), bbox_inches='tight', transparent="True", pad_inches=0.1)
            plt.close()

    for i in range(3):
        trace_id = "S" + str(i+1)
        for length in range(4):
            for j in range(len(experiments)):
                experiment_path = experiments[j]
                plt.plot(average_traced_events[experiment_path][length+1][trace_id]["flat"], label=labels[j], marker=markers[j], markevery=1000000)
            plt.legend()
            plt.ticklabel_format(style="plain")
            plt.xlabel("Total Events")
            plt.ylabel("Traced Events")

            plt.savefig("Average_Traced_Events_{}_{}_flat.pdf".format(str(length+1), trace_id), bbox_inches='tight', transparent="True", pad_inches=0.1)
            plt.close()

if __name__ == "__main__":
    main(sys.argv)
