#!/bin/bash

wd=$pwd
services_file=$wd/services.json
trace_length=10000000

services="notification_service statistics_service ts_food_map_service account_service ts_contacts_service ts_consign_price_service"

for service in $services
do
    . $wd/run_trace_generation.sh &
done
wait