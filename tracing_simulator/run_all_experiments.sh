#!/bin/bash

wd=$pwd
command=$wd/Tracer.py
traces_folder=$wd/traces
perfect_tracing_folder=$wd/perfect_tracing

number_of_runs=20
run_for=10000000

services4="notification_service statistics_service ts_food_map_service account_service"
services3="ts_contacts_service ts_consign_price_service"

seq_length4="1 2 3 4"
seq_length3="1 2 3"

# LAZY
p_min=0.01
p_max=0.05
update_rate=1000

for service in $services4
do
    seq_length=$seq_length4
    . $wd/run_experiment.sh &
done

for service in $services3
do
    seq_length=$seq_length3
    . $wd/run_experiment.sh &
done

wait

# MEDIUM
p_min=0.03
p_max=0.1
update_rate=1000

for service in $services4
do
    seq_length=$seq_length4
    . $wd/run_experiment.sh &
done

for service in $services3
do
    seq_length=$seq_length3
    . $wd/run_experiment.sh &
done

wait

# EAGER
p_min=0.05
p_max=0.2
update_rate=1000
for service in $services4
do
    seq_length=$seq_length4
    . $wd/run_experiment.sh &
done

for service in $services3
do
    seq_length=$seq_length3
    . $wd/run_experiment.sh &
done

