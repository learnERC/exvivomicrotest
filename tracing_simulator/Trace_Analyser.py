from Sequence_Generator import Sequence_Generator
import sys, pickle, os, json
from itertools import tee


def window(iterable, size):
    iters = tee(iterable, size)
    for i in range(1, size):
        for each in iters[i:]:
            next(each, None)
    return zip(*iters)


def main(argv):

    trace_to_execute_path = argv[1]
    L_max = int(argv[2])

    with open(trace_to_execute_path, "r") as infile:
        trace_to_execute = json.load(infile)
    
    output_base_name = os.path.basename(trace_to_execute_path)
    output_base_name = output_base_name[:output_base_name.rindex(".")]

    for i in range(L_max, L_max+1):
        covered_sequences = set()
        covered_sequences_count = list()

        for sequence in window(trace_to_execute, i):
            if Sequence_Generator._s_validate_sequence(sequence):
                covered_sequences.add(sequence)
            covered_sequences_count.append(len(covered_sequences))
        
        for j in range(len(covered_sequences_count), len(trace_to_execute)):
            covered_sequences_count.append(len(covered_sequences))
        
        with open("Perfect_tracing_{}_{}.p".format(output_base_name, i), "wb") as outfile:
            pickle.dump(covered_sequences_count, outfile, pickle.HIGHEST_PROTOCOL)
        print(len(covered_sequences_count))
        print("sequences of lenght {} in trace: {}".format(i, len(covered_sequences)))
    


if __name__ == "__main__":
    main(sys.argv)