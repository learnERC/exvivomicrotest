from Sequence_Generator import Sequence_Generator
import sys, random, os, json

from Tracer import Tracer
from os import listdir
from os.path import isfile, join
  
def _read_scenarios_files(files_list):
    to_ret = list()
    for scenario_file in files_list:
        my_file = open(scenario_file, "r")
        scenario_list = my_file.read().splitlines()
        scenario_list = [s for s in scenario_list if s]
        status_code_index = 2
        new_scenario_list = list()
        for event in scenario_list:
            if Tracer._is_request(event) or event[status_code_index] == "1":
                new_scenario_list.append(event)
            else: 
                new_scenario_list.append(event[:status_code_index+1])    
        
        to_ret.append(new_scenario_list)
    return to_ret

def load_scenarios(service_scenarios_folder_path):
    scenarios_files_list = [os.path.join(service_scenarios_folder_path, f) for f in listdir(service_scenarios_folder_path) if isfile(join(service_scenarios_folder_path, f)) and f.endswith(".log")]
    scenarios_list = _read_scenarios_files(scenarios_files_list)
    return scenarios_list

def __get_random_scenario(scenarios_list, random_sequence = False, generator = False):
    if random_sequence:
        random_scenario_index = random.randint(0, len(scenarios_list))
        if random_scenario_index == len(scenarios_list):
            return list(generator.get_random_valid_sequence(2)), "random"
    random_scenario_index = random.randint(0, len(scenarios_list)-1)
    scenario = scenarios_list[random_scenario_index]
    return scenario, random_scenario_index

def __combinations_trace(number_of_events, service_scenarios_folder_path, random_events, generator):
    trace = list()
    scenarios_list = load_scenarios(service_scenarios_folder_path)
    i = 0
    #### 
    scenarios_counter_dict = {}
    for x in range(len(scenarios_list)):
        scenarios_counter_dict[x] = 0
    scenarios_counter_dict["random"] = 0
    ###
    while i < number_of_events:
        scenario, scenario_index = __get_random_scenario(scenarios_list)
        scenarios_counter_dict[scenario_index] += 1
        for event in scenario:
            if i == number_of_events:
                break
            trace.append(event)
            i += 1
        if random_events and i < number_of_events:
            random_sequence = generator.get_random_valid_sequence(2)
            scenarios_counter_dict["random"] += 1

            trace.append(random_sequence[0])
            i += 1
            if i < number_of_events:
                trace.append(random_sequence[1])
                i += 1
    return trace, scenarios_counter_dict

def get_combinations_trace(number_of_events, service_scenarios_folder_path):
    return __combinations_trace(number_of_events, service_scenarios_folder_path, random_events = False, generator = False)

def get_combinations_trace_with_random_events(number_of_events, service_scenarios_folder_path, generator):
    return __combinations_trace(number_of_events, service_scenarios_folder_path, True, generator)

def get_interleaved_traces(number_of_events, service_scenarios_folder_path, generator):
    scenarios_list = load_scenarios(service_scenarios_folder_path)
    i = 0
    trace = list()
    running_scenarios_list = list()
    #### 
    scenarios_counter_dict = {}
    for x in range(len(scenarios_list)):
        scenarios_counter_dict[x] = 0
    scenarios_counter_dict["random"] = 0
    ###
    while i < number_of_events:
        prob_to_start_new_scenario = 1 / (1 + len(running_scenarios_list))
        prob = random.random()
        if prob_to_start_new_scenario > prob:
            scenario, scenario_index = __get_random_scenario(scenarios_list, True, generator)
            new_scenario = scenario.copy()
            scenarios_counter_dict[scenario_index] += 1
            trace.append(new_scenario.pop(0))
            running_scenarios_list.append(new_scenario)
            i += 1
        else:
            running_scenario, scenario_index = __get_random_scenario(running_scenarios_list)
            trace.append(running_scenario.pop(0))
            i += 1
            if len(running_scenario) == 0:
                running_scenarios_list.remove(running_scenario)
    return trace, scenarios_counter_dict


# (1) Regular

# (2) With Random Events

# (3) Overlapped

def main(argv): 
    services_definitions = argv[1]
    service = argv[2]
    #length = argv[3]
    service_scenarios_folder_path = argv[3]
    runtime = int(argv[4])
    

    generator = Sequence_Generator(services_definitions, service)
    combinations_trace, use_cases_dict = get_combinations_trace(runtime, service_scenarios_folder_path)
    file_name = os.path.basename(service_scenarios_folder_path) + "_S1.trace"
    with open(file_name, "w") as outfile:
        json.dump(combinations_trace, outfile)
    print(len(combinations_trace))
    print(str(use_cases_dict))
    del combinations_trace

    combinations_random_trace, use_cases_dict = get_combinations_trace_with_random_events(runtime, service_scenarios_folder_path, generator)
    file_name = os.path.basename(service_scenarios_folder_path) + "_S2.trace"
    with open(file_name, "w") as outfile:
        json.dump(combinations_random_trace, outfile)
    print(len(combinations_random_trace))
    print(str(use_cases_dict))
    del combinations_random_trace

    interleaved_random_trace, use_cases_dict = get_interleaved_traces(runtime, service_scenarios_folder_path, generator)
    file_name = os.path.basename(service_scenarios_folder_path) + "_S3.trace"
    with open(file_name, "w") as outfile:
        json.dump(interleaved_random_trace, outfile)
    print(len(interleaved_random_trace))
    print(str(use_cases_dict))


if __name__ == "__main__":
    main(sys.argv)