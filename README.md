# ExVivoMicroTest: Ex-Vivo Testing of Microservices

## Synopsis
Replication package of the paper ExVivoMicroTest: Ex-Vivo Testing of Microservices.

## Run simulations

**Generate request - response symbol traces**

```shell
./run_all_scenarios_logger_monitor.sh
```
This script produces log files in the `{piggymetics|train-ticket}/scenarios/{service}` directory contaning the request - response symbol traces.
These log files are used by the simulator to produce Regular, WithRandom and Overlapped scenario traces.

**Generate request - response spaces definitions**
```shell
./tracing_simulation/run_all_sequence_generation.sh
```
This script produces the spaces of all possibile sequences of length `N` for the services defined in the `services.json` file. This file is a JSON representation of the services abstraction.

**Generate scenario traces**
```shell
./tracing_simulation/run_all_trace_generation.sh
```
This script exploits the the scenarios log files and the definitions of all the admissibile sequences to produce the Regular, WithRandom and Overlapped scenario traces for all services. The scenarios must be placed in folders matching the services names, all within a folder named `scenarios` in the working directory of the script.

**Analysis of scenario traces**
```shell
./tracing_simulation/run_all_trace_analysis.sh
```
This script analyses the scenario traces in order to identify how many sequences of length `N` are present in each trace.

**Run tracing experiments**
```shell
./tracing_simulation/run_all_experiments.sh
```
This script runs the tracing experiments for all the services and corresponding sequences lengths. For each service 20 runs of each experiment setup (Lazy, Regular, and Eager) are executed and averaged. Metrics regarding the covered sequences and the numbers of traced events for each service-configuration pair are produced and plotted.

**Run experiments analysis**
```python
./tracing_simulation/Experiment_Analysis.py
```
This python script aggregates the data produced in the experiments and creates all the plots used in the paper and in the online appendix.

## Run overhead experiments

### Requirements

- A Linux machine (Microsoft Azure VM with 4 vCPUs, 16 GiB RAM and 512GiB SSD, OS Debian 5.9 OS)
- tcpdump (version 4.9.3)
- libpcap (version 1.8.1)
- Docker (version 20.10.2, build 2291f61)
- Docker Compose (version 1.27.4, build 40524192)
- Metricbeat (version 7.10.2)
- Logstash (7.1.1)

### Configuring Metricbeat and Logstash

**Install Logstash CSV output plugin**

Usually Logstash binaries are located at `/usr/share/logstash/bin` (see [Directory Layout of Debian and RPM Packages](https://www.elastic.co/guide/en/logstash/current/dir-layout.html#deb-layout), change the command below if needed.

```shell
sudo ./usr/share/logstash/bin/logstash-plugin install logstash-output-csv
```

**Configure CSV output**

Copy the `logstash-csv.conf` file provided into `/etc/logstash/conf.d` directory.

**Configure Metricbeat**

Replace the `/etc/metricbeat/metricbeat.yml` file with the one provided.

Delete the `/etc/metricbeat/modules.d/docker.yml.disabled` file and copy the `docker.yml` file provided in the same directory.

### Scripts

Each system directory (piggymetrics, train-ticket) contains the following scripts.

**Collect baseline resource consumption**

```shell
./run_all_scenarios_overhead_baseline.sh number_of_runs
```

This script produces latency log files in the `{piggymetics|train-ticket}/overhead-experiments/baseline/{service}/run-{i}` directory.
CSV files contaning services CPU e memory consumption data are placed in the Logstash home directory, which is usually `/usr/share/logstash`.

> NOTE: CSV files MUST be moved to another directory before to run other experiments. Otherwise, new data will be appended to the same CSV file.


**Collect monitor resource consumption**

```shell
./run_all_scenarios_overhead_monitor.sh number_of_runs
```
This script produces latency log files in the `{piggymetics|train-ticket}/overhead-experiments/monitor/{service}/run-{i}` directory.
CSV files contaning services CPU e memory consumption data are placed in the Logstash home directory, which is usually `/usr/share/logstash`.

> NOTE: CSV files MUST be moved to another directory before to run other experiments. Otherwise, new data will be appended to the same CSV file.

**Collect tracing resource consumption**

```shell
./run_all_scenarios_overhead_tracing.sh number_of_runs
```
This script produces latency log files in the `{piggymetics|train-ticket}/overhead-experiments/tracing/{service}/run-{i}` directory.
CSV files contaning services CPU e memory consumption data are placed in the Logstash home directory, which is usually `/usr/share/logstash`.

> NOTE: CSV files MUST be moved to another directory before to run other experiments. Otherwise, new data will be appended to the same CSV file.


