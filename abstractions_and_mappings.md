﻿# Abstractions
| Variable | Abstraction | Mapping |
| -- | -- | -- |
| Numerical values | NULL \| lt0 \| gt0 \| eq0 | 0 \| 1 \| 2 \| 3 |
| String values | NULL \| empty \| notEmpty | 0 \| 1 \| 2 |
| Object refs | NULL \| notNULL (instanceof Class C) | 0 \| 1 |
| Enums | NULL \| all e in E | 0...e |
| Boolean | NULL \| true \| false | 0 \| 1 \| 2 |
| Lists/Maps | NULL \| empty \| notEmpty | 0 \| 1 \| 2 |

<br>

| Req/Res | Abstraction |
| -- | -- |
| Incoming request | { request prefix }{ endpoint }{ query params }{ payload } |
| Outgoing response | { response prefix }{ endpoint }{ query params }{ status code }{ payload } |

<br>

| Prefixes | Abstraction |
| -- | -- |
| Request prefix | 0 |
| Response prefix | 1 |

<br>

| Status Code | Abstraction |
| -- | -- |
| 1xx | 0 |
| 2xx | 1 |
| 3xx | 2 |
| 4xx | 3 |
| 5xx | 4 |

<br>

## Piggymetrics

| Classes and Attributes | Abstraction | Mapping |
| -- | -- | -- |
| Account (account-service) | { string }{ object ref }{ list }{ list }{ account.saving }{ account.note } | - |
| Account (statistics-service) | { list }{ list }{ account.saving } | - |
| Recipient | { string }{ recipient.email }{ map } | - |
| User | { user.username }{ user.password } | - |
| Account.saving | NULL \| notNULL | 0 \| 1|
| Account.note | NULL \| lte20000 \| gt20000 | 0 \| 1 \| 2 |
| Recipient.email | NULL \| notNULL | 0 \| 1|
| User.username | NULL \| gte3 and lte20 \| other | 0 \| 1 \| 2 |
| User.password | NULL \| gte6 and lte40 \| other | 0 \| 1 \| 2 |

<br>

### notification-service
**GET /notifications/recipients/current**

_Request_: 00

_Response_: 10{ status code }{ payload }

- endpoint: 0
- status code: 2xx | 4xx | 5xx
- payload: Recipient class

**PUT /notifications/recipients/current**:

_Request_: 01{ payload }

_Response_: 11{ status code }{ payload }

- endpoint: 1
- status code: 2xx | 4xx | 5xx
- payload: Recipient

<br>

### account-service
**GET /accounts/{name}**

_Request_: 00

_Response_: 10{ status code }{ payload }

- endpoint: 0
- status code: 2xx | 4xx | 5xx
- payload: Account (account-service)

**GET /accounts/current**:

_Request_: 01

_Response_: 11{ status code }{ payload }

- endpoint: 1
- status code: 2xx | 4xx | 5xx
- payload: Account (account-service)

**PUT /accounts/current**:

_Request_: 02{ payload }

_Response_: 12{ status code }

- endpoint: 2
- status code: 2xx | 4xx | 5xx
- payload: Account (account-service)

**POST /accounts**:

_Request_: 03{ payload }

_Response_: 13{ status code }{ payload }

- endpoint: 3
- status code: 2xx | 4xx | 5xx
- payload request: User
- payload response: Account (account-service)

<br>

### statistics-service

**GET /statistics/current**:

_Request_: 00

_Response_: 10{ status code }{ payload }

- endpoint: 0
- status code: 2xx | 4xx | 5xx
- payload: List

**GET /statistics/{accountName}**

_Request_: 01

_Response_: 11{ status code }{ payload }

- endpoint: 1
- status code: 2xx | 4xx | 5xx
- payload: List

**PUT /statistics/{accountName}**:

_Request_: 02{ payload }

_Response_: 12{ status code }

- endpoint: 2
- status code: 2xx | 4xx | 5xx
- payload: Account (statistics-service)

<br>

## Train Ticket

| Classes and Attributes | Abstraction | Mapping |
| -- | -- | -- |
| Contacts | { object ref }{ object ref }{ string }{ numerical }{ string }{ string } | - |
| ConsignPrice | { object ref }{ numerical }{ numerical }{ numerical }{ numerical }{ numerical } | - |


### ts-contacts-service

**GET /api/v1/contactservice/contacts**

_Request_: 00

_Response_: 10{ status code }{ payload }

- endpoint: 0
- status code: 2xx | 4xx | 5xx
- payload: List

**POST /api/v1/contactservice/contacts**

_Request_: 01{ payload }

_Response_: 11{ status code }{ payload }

- endpoint: 1
- status code: 2xx | 4xx | 5xx
- payload: Contacts | NULL

**POST /api/v1/contactservice/contacts/admin**

_Request_: 02{ payload }

_Response_: 12{ status code }{ payload }

- endpoint: 2
- status code: 2xx | 4xx | 5xx
- payload: Contacts | NULL

**DELETE /api/v1/contactservice/contacts/{contactsId}**

_Request_: 03

_Response_: 13{ status code }{ payload }

- endpoint: 3
- status code: 2xx | 4xx | 5xx
- payload: Object Ref

**PUT /api/v1/contactservice/contacts**

_Request_: 04{ payload }

_Response_: 14{ status code }{ payload }

- endpoint: 4
- status code: 2xx | 4xx | 5xx
- payload: Contacts | NULL

**GET /api/v1/contactservice/contacts/account/{accountId}**

_Request_: 05

_Response_: 15{ status code }{ payload }

- endpoint: 5
- status code: 2xx | 4xx | 5xx
- payload: List

**GET /api/v1/contactservice/contacts/{id}**

_Request_: 06

_Response_: 16{ status code }{ payload }

- endpoint: 6
- status code: 2xx | 4xx | 5xx
- payload: Contacts | NULL

<br>

### ts-consign-price-service

**GET /api/v1/consignpriceservice/consignprice/{weight}/{isWithinRegion}**

_Request_: 00

_Response_: 10{ status code }{ payload }

- endpoint: 0
- status code: 2xx | 4xx | 5xx
- payload: ConsignPrice | NULL

**GET /api/v1/consignpriceservice/consignprice/price**

_Request_: 01

_Response_: 11{ status code }{ payload }

- endpoint: 1
- status code: 2xx | 4xx | 5xx
- payload: string

**GET /api/v1/consignpriceservice/consignprice/config**

_Request_: 02

_Response_: 12{ status code }{ payload }

- endpoint: 2
- status code: 2xx | 4xx | 5xx
- payload: ConsignPrice | NULL

**POST /api/v1/consignpriceservice/consignprice**

_Request_: 03{ payload }

_Response_: 13{ status code }{ payload }

- endpoint: 3
- status code: 2xx | 4xx | 5xx
- payload: ConsignPrice

<br>

### ts-food-map-service

**GET /api/v1/foodmapservice/foodstores**

_Request_: 00

_Response_: 10{ status code }{ payload }

- endpoint: 0
- status code: 2xx | 5xx
- payload: List

**GET /api/v1/foodmapservice/foodstores/{stationId}**

_Request_: 01

_Response_: 11{ status code }{ payload }

- endpoint: 1
- status code: 2xx | 5xx
- payload: List

**POST /api/v1/foodmapservice/foodstores**

_Request_: 02{ payload }

_Response_: 12{ status code }{ payload }

- endpoint: 2
- status code: 2xx | 5xx
- payload: List

**GET /api/v1/foodmapservice/trainfoods**

_Request_: 03

_Response_: 13{ status code }{ payload }

- endpoint: 3
- status code: 2xx | 5xx
- payload: List

**GET /api/v1/foodmapservice/trainfoods/{tripId}**

_Request_: 04

_Response_: 14{ status code }{ payload }

- endpoint: 4
- status code: 2xx | 5xx
- payload: List